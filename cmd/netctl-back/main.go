// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"fmt"
	"os"
	"os/signal"
	"strings"
	"syscall"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	"gitlab.com/redfield/netctl/pkg/backend"
)

const (
	backendDescription = `netctl backend provides backend services for frontends,
such as storing network configurations, and providing UI proxies`
)

var (
	rootCmd = &cobra.Command{
		Use:   "netctl-back",
		Short: "netctl backend service",
		Long:  backendDescription,
		PreRunE: func(cmd *cobra.Command, args []string) error {
			if err := viper.BindPFlags(cmd.Flags()); err != nil {
				return err
			}
			return nil
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			opts := make([]backend.ServerOption, 0)

			if viper.GetBool("disable-database") {
				opts = append(opts, backend.WithNoDatabase())
			}

			if viper.GetBool("enable-dbus-proxies") {
				opts = append(opts, backend.WithDBusProxies())
			}

			if path := viper.GetString("database-path"); path != "" {
				opts = append(opts, backend.WithDatabasePath(path))
			}

			b, err := backend.NewBackend(viper.GetString("address"), opts...)
			if err != nil {
				return err
			}

			ec := make(chan error)
			go func() {
				ec <- b.Serve()
			}()

			defer b.Close()

			c := make(chan os.Signal, 1)
			signal.Notify(c, syscall.SIGTERM)

			select {
			case err := <-ec:
				return err

			case <-c:
				return nil
			}
		},
	}
)

func init() {
	viper.SetEnvPrefix("NETCTL")
	viper.SetEnvKeyReplacer(strings.NewReplacer("-", "_"))
	viper.AutomaticEnv()

	rootCmd.Flags().String("address", "tcp://:50051", "Server address (tcp://host:port or unix:///path)")

	rootCmd.Flags().Bool("disable-database", false, "Disables the database on the backend")
	rootCmd.Flags().String("database-path", "", "Specify non-default database path")
	rootCmd.Flags().Bool("enable-dbus-proxies", false, "Enables DBus proxy services for frontends")
}

func main() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println("Error creating backend: ", err)
		os.Exit(-1)
	}
}
