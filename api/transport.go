// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package api

import (
	"fmt"
	"net/url"
	"strings"

	"github.com/pkg/errors"
	"google.golang.org/grpc"
)

const (
	transportTCP  = "tcp"
	transportUnix = "unix"
)

// Transport represents a transport specifier for netctl.
type Transport struct {
	Info *TransportInfo
}

// ParseTransport returns a new transport by parsing the given
// dest string. An error is returned if the scheme is not supported
// or the dest cannot be parsed.
func ParseTransport(dest string) (*Transport, error) {
	u, err := url.Parse(dest)
	if err != nil {
		return nil, errors.Wrap(err, "failed to parse dest string")
	}

	switch u.Scheme {
	case transportTCP:
		// According to https://golang.org/pkg/net/url/#URL.Hostname,
		// Hostname always strips the port number if present. So, we
		// can use the presence of ':' characters in the hostname to
		// indicate an IPv6 address.
		if strings.Contains(u.Hostname(), ":") {
			return NewIPv6Transport(u.Hostname(), u.Port()), nil
		}

		return NewIPv4Transport(u.Hostname(), u.Port()), nil

	case transportUnix:
		return NewSocketTransport(u.Path), nil

	default:
		return nil, errors.Errorf("unsupported transport type '%v'", u.Scheme)
	}
}

// NewIPv4Transport returns a Transport for IPv4.
func NewIPv4Transport(addr, port string) *Transport {
	ti := &TransportInfo{
		Type: TransportInfo_IPV4,
		Destination: &TransportInfo_Ipv4{
			&DestinationIPv4{
				Address: addr,
				Port:    port,
			},
		},
	}

	return &Transport{ti}
}

// NewIPv6Transport returns a Transport for IPv6.
func NewIPv6Transport(addr, port string) *Transport {
	ti := &TransportInfo{
		Type: TransportInfo_IPV6,
		Destination: &TransportInfo_Ipv6{
			&DestinationIPv6{
				Address: addr,
				Port:    port,
			},
		},
	}

	return &Transport{ti}
}

// NewSocketTransport returns a Transport for Unix domain sockets.
func NewSocketTransport(path string) *Transport {
	ti := &TransportInfo{
		Type: TransportInfo_SOCKET,
		Destination: &TransportInfo_Socket{
			&DestinationSocket{
				Path: path,
			},
		},
	}

	return &Transport{ti}
}

// ClientConn creates a new grpc.ClientConn from the Transport.
func (t Transport) ClientConn() (*grpc.ClientConn, error) {
	var target string

	// grpc.Dial doesn't seem to play well with the net.Addr
	// interface. Namely, passing scheme://name works for Unix,
	// but not for TCP.
	//
	// Make sure the target string is well-formatted before calling
	// Dial.
	switch t.Network() {
	case transportUnix:
		target = fmt.Sprintf("%s://%s", t.Network(), t.String())
	case transportTCP:
		target = t.String()
	}

	return grpc.Dial(target, grpc.WithInsecure())
}

// Network returns the network name.
func (t Transport) Network() string {
	switch t.Info.GetType() {
	case TransportInfo_IPV4, TransportInfo_IPV6:
		return transportTCP
	case TransportInfo_SOCKET:
		return transportUnix
	default:
		return ""
	}
}

// String returns the string form of the destination address.
func (t Transport) String() string {
	switch t.Info.GetType() {
	case TransportInfo_IPV4:
		return t.Info.GetIpv4().transportString()
	case TransportInfo_IPV6:
		return t.Info.GetIpv6().transportString()
	case TransportInfo_SOCKET:
		return t.Info.GetSocket().transportString()
	default:
		return ""
	}
}

func (d DestinationIPv4) transportString() string {
	if d.Address != "" && d.Port != "" {
		return fmt.Sprintf("%v:%v", d.Address, d.Port)
	}

	if d.Address != "" {
		return d.Address
	}

	if d.Port != "" {
		return fmt.Sprintf(":%v", d.Port)
	}

	return ""
}

func (d DestinationIPv6) transportString() string {
	if d.Address != "" && d.Port != "" {
		return fmt.Sprintf("[%v]:%v", d.Address, d.Port)
	}

	if d.Address != "" {
		return d.Address
	}

	if d.Port != "" {
		return fmt.Sprintf("[::]:%v", d.Port)
	}

	return ""
}

func (d DestinationSocket) transportString() string {
	return d.Path
}
