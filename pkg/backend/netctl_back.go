// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package backend

import (
	"context"
	"log"
	"net"
	"os"
	"sync"

	"github.com/pkg/errors"
	"google.golang.org/grpc"

	"gitlab.com/redfield/netctl/api"
	"gitlab.com/redfield/netctl/pkg/ui"
)

// Backend implements a netctl_back service.
type Backend struct {
	// Maintain a map of registered frontends, and
	// one for their associated UI proxies. Protect access to
	// the maps with a lock.
	mux       sync.Mutex
	frontends map[string]*api.FrontendMetaData
	uiProxies map[string]ui.Proxy

	ti *api.Transport

	// gRPC server
	server *grpc.Server

	// Database for the service
	*database

	// Backend options
	*serverOptions
}

// NewBackend returns a new netctl_back service.
func NewBackend(addr string, opts ...ServerOption) (*Backend, error) {
	ti, err := api.ParseTransport(addr)
	if err != nil {
		return nil, errors.Wrap(err, "invald address")
	}

	b := &Backend{
		frontends:     make(map[string]*api.FrontendMetaData),
		uiProxies:     make(map[string]ui.Proxy),
		ti:            ti,
		serverOptions: &serverOptions{},
	}

	for _, opt := range opts {
		opt.apply(b.serverOptions)
	}

	if !b.withoutDatabase {
		db, err := createDatabase(b.databasePath)
		if err != nil {
			return nil, errors.Wrap(err, "failed to create database")
		}

		b.database = db
	}

	return b, nil
}

// ServerOption is used to configure a netctl backend.
type ServerOption interface {
	apply(*serverOptions)
}

type serverOptions struct {
	// Database path, if database is enabled.
	databasePath string

	// Do not create the database if this is set.
	withoutDatabase bool

	// Use DBus for UI proxy.
	withDBusProxies bool
}

type funcServerOption struct {
	f func(*serverOptions)
}

func (fso *funcServerOption) apply(s *serverOptions) {
	fso.f(s)
}

func newFuncServerOption(f func(*serverOptions)) *funcServerOption {
	return &funcServerOption{f}
}

// WithNoDatabase tells the backend to start without a database.
func WithNoDatabase() ServerOption {
	return newFuncServerOption(func(s *serverOptions) {
		s.withoutDatabase = true
	})
}

// WithDatabasePath specifies the database path to use. If this is not
// specified, a default path is used.
func WithDatabasePath(path string) ServerOption {
	return newFuncServerOption(func(s *serverOptions) {
		s.databasePath = path
	})
}

// WithDBusProxies tells the backend to use DBus for UI proxies.
func WithDBusProxies() ServerOption {
	return newFuncServerOption(func(s *serverOptions) {
		s.withDBusProxies = true
	})
}

// Serve starts the netctl back service.
func (b *Backend) Serve() error {
	lis, err := b.newListener()
	if err != nil {
		return errors.Wrap(err, "failed to create listener")
	}
	defer lis.Close()

	b.server = grpc.NewServer()

	api.RegisterNetctlBackServer(b.server, b)

	return b.server.Serve(lis)
}

func (b *Backend) newListener() (net.Listener, error) {
	switch b.ti.Network() {
	case "tcp":
		return net.Listen(b.ti.Network(), b.ti.String())
	case "unix":
		lis, err := net.Listen(b.ti.Network(), b.ti.String())
		if err != nil {
			return lis, err
		}

		// Set permissions of the socket so that is world-writable.
		if err := os.Chmod(b.ti.String(), 0777); err != nil {
			lis.Close()

			return nil, errors.Wrap(err, "failed to create socket listener")
		}

		return lis, nil
	default:
		return nil, errors.Errorf("unknown network type %s", b.ti.Network())
	}
}

// Close tears down the Backend.
func (b *Backend) Close() error {
	if b.server != nil {
		b.server.GracefulStop()
	}

	// Don't return an error until we have tried to go through
	// every frontend.
	var (
		failedNotify     bool
		failedUITeardown bool
	)

	for _, nf := range b.getAllFrontends() {
		err := b.notifyBackendClosing(nf)
		if err != nil {
			failedNotify = true
		}

		err = b.teardownUIProxy(nf)
		if err != nil {
			failedUITeardown = true
		}

		b.removeFrontend(nf.Uuid)
	}

	if failedNotify || failedUITeardown {
		return errors.New("failed to clean up all frontends")
	}

	return nil
}

func (b *Backend) notifyBackendClosing(nf *api.FrontendMetaData) error {
	nfc, err := createFrontendClient(nf)
	if err != nil {
		return errors.Wrap(err, "failed to create frontend client")
	}
	defer nfc.Close()

	notification := &api.Notification{
		FrontUuid: nf.Uuid,
		Type:      api.Notification_BACKEND_LEAVING,
	}
	err = nfc.Notify(notification)
	if err != nil {
		return errors.Wrap(err, "failed to notify frontend")
	}

	return nil
}

// RegisterFrontend registers a netctl frontend with the backend.
func (b *Backend) RegisterFrontend(ctx context.Context, r *api.RegisterFrontendRequest) (*api.RegisterFrontendReply, error) {
	nf := r.GetFrontend()
	if nf == nil {
		return nil, errors.New("did not receive frontend information")
	}

	ti := nf.GetTransportInfo()
	if ti == nil {
		return nil, errors.New("did not receive transport info for frontend")
	}

	err := b.initializeFrontend(nf)
	if err != nil {
		return nil, errors.Wrap(err, "failed to initialize frontend")
	}

	b.initializeUIProxy(nf)

	// Initialize the DB for this frontend.
	if !b.withoutDatabase {
		if err := b.createFrontendPath(nf.Uuid); err != nil {
			log.Printf("Failed to initialize db for %s: %v", nf.Uuid, err)
		}
	}

	log.Printf("%v frontend registered with UUID %v", nf.Type, nf.Uuid)

	return &api.RegisterFrontendReply{}, nil
}

func (b *Backend) initializeFrontend(nf *api.FrontendMetaData) error {
	nfc, err := createFrontendClient(nf)
	if err != nil {
		return errors.Wrap(err, "failed to create frontend client")
	}
	defer nfc.Close()

	notification := &api.Notification{
		FrontUuid: nf.Uuid,
		Type:      api.Notification_REGISTER_ACK,
	}
	err = nfc.Notify(notification)
	if err != nil {
		return errors.Wrap(err, "failed to ACK frontend registration")
	}

	b.addFrontend(nf)

	return nil
}

func (b *Backend) initializeUIProxy(nf *api.FrontendMetaData) {
	if !b.withDBusProxies {
		return
	}

	b.addUIProxy(nf)

	go func() {
		err := b.serveUIProxy(nf.Uuid)
		if err != nil {
			log.Printf("Failed to serve UI proxy: %v", err)
		}
	}()
}

func (b *Backend) serveUIProxy(uuid string) error {
	nf, err := b.getFrontend(uuid)
	if err != nil {
		return err
	}

	nfc, err := createFrontendClient(nf)
	if err != nil {
		return err
	}

	proxy, err := b.getUIProxy(uuid)
	if err != nil {
		return err
	}

	return proxy.Serve(nfc)
}

func createFrontendClient(nf *api.FrontendMetaData) (*api.FrontendClient, error) {
	ti := &api.Transport{Info: nf.TransportInfo}

	if ti.String() == "" {
		return nil, errors.New("transport unspecified")
	}

	cc, err := ti.ClientConn()
	if err != nil {
		return nil, errors.Wrap(err, "failed to create client conn")
	}

	fc := api.NewFrontendClient(cc)

	return fc, nil
}

func (b *Backend) addFrontend(nf *api.FrontendMetaData) {
	b.mux.Lock()
	defer b.mux.Unlock()

	b.frontends[nf.Uuid] = nf
}

func (b *Backend) getFrontend(uuid string) (*api.FrontendMetaData, error) {
	b.mux.Lock()
	defer b.mux.Unlock()

	nf, ok := b.frontends[uuid]
	if !ok {
		return nil, errors.New("no such frontend")
	}

	return nf, nil
}

func (b *Backend) getAllFrontends() []*api.FrontendMetaData {
	b.mux.Lock()
	defer b.mux.Unlock()

	frontends := make([]*api.FrontendMetaData, 0)

	for _, frontend := range b.frontends {
		frontends = append(frontends, frontend)
	}

	return frontends
}

func (b *Backend) removeFrontend(uuid string) {
	b.mux.Lock()
	defer b.mux.Unlock()

	delete(b.frontends, uuid)
}

func (b *Backend) addUIProxy(nf *api.FrontendMetaData) {
	b.mux.Lock()
	defer b.mux.Unlock()

	if b.withDBusProxies {
		b.uiProxies[nf.Uuid] = ui.NewDBusProxy(nf.Type)
	}
}

func (b *Backend) getUIProxy(uuid string) (ui.Proxy, error) {
	b.mux.Lock()
	defer b.mux.Unlock()

	proxy, ok := b.uiProxies[uuid]
	if !ok {
		return nil, errors.New("UI proxy does not exist")
	}

	return proxy, nil
}

func (b *Backend) removeUIProxy(uuid string) {
	b.mux.Lock()
	defer b.mux.Unlock()

	delete(b.uiProxies, uuid)
}

// UnregisterFrontend unregisters a frontend from the backend.
func (b *Backend) UnregisterFrontend(ctx context.Context, r *api.UnregisterFrontendRequest) (*api.UnregisterFrontendReply, error) {
	nf := r.GetFrontend()
	if nf == nil {
		return nil, errors.New("did not receive frontend information")
	}

	err := b.teardownUIProxy(nf)
	if err != nil {
		return nil, errors.Wrap(err, "failed to teardown UI proxy")
	}

	err = b.teardownFrontend(nf)
	if err != nil {
		return nil, errors.Wrap(err, "failed to teardown frontend")
	}

	log.Printf("%v frontend with UUID %v unregistered", nf.Type, nf.Uuid)

	return &api.UnregisterFrontendReply{}, nil
}

func (b *Backend) teardownFrontend(nf *api.FrontendMetaData) error {
	_, err := b.getFrontend(nf.Uuid)
	if err != nil {
		return err
	}

	b.removeFrontend(nf.Uuid)

	return nil
}

func (b *Backend) teardownUIProxy(nf *api.FrontendMetaData) error {
	if !b.withDBusProxies {
		return nil
	}

	proxy, err := b.getUIProxy(nf.Uuid)
	if err != nil {
		return err
	}

	proxy.Close()
	b.removeUIProxy(nf.Uuid)

	return nil
}

// GetAllFrontends returns a list of registered frontends.
func (b *Backend) GetAllFrontends(ctx context.Context, r *api.GetAllFrontendsRequest) (*api.GetAllFrontendsReply, error) {
	frontends := &api.GetAllFrontendsReply{
		Frontends: b.getAllFrontends(),
	}

	return frontends, nil
}

// GetFrontend returns a frontend matching the specified UUID.
func (b *Backend) GetFrontend(ctx context.Context, r *api.GetFrontendRequest) (*api.GetFrontendReply, error) {
	nf, err := b.getFrontend(r.GetUuid())
	if err != nil {
		return nil, errors.Wrap(err, "failed to find frontend")
	}

	frontend := &api.GetFrontendReply{
		Frontend: nf,
	}

	return frontend, nil
}

// SaveNetwork caches a network provided by a frontend.
func (b *Backend) SaveNetwork(ctx context.Context, r *api.SaveNetworkRequest) (*api.SaveNetworkReply, error) {
	if b.withoutDatabase {
		return nil, errors.New("database is not enabled")
	}

	uuid := r.GetFrontend().GetUuid()

	err := b.saveNetwork(uuid, r.GetNetwork())
	if err != nil {
		return nil, errors.Wrap(err, "failed to write network configuration")
	}

	return &api.SaveNetworkReply{}, nil
}

// GetSavedNetworks returns a list of cached network for a given frontend..
func (b *Backend) GetSavedNetworks(ctx context.Context, r *api.GetSavedNetworksRequest) (*api.GetSavedNetworksReply, error) {
	if b.withoutDatabase {
		return nil, errors.New("database is not enabled")
	}

	uuid := r.GetFrontend().GetUuid()

	networks, err := b.getSavedNetworks(uuid)
	if err != nil {
		return nil, err
	}

	return &api.GetSavedNetworksReply{Networks: networks}, nil
}
