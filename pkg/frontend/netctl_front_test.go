package frontend

import (
	"context"
	"log"
	"net"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/pkg/errors"
	"google.golang.org/grpc"

	"gitlab.com/redfield/netctl/api"
	"gitlab.com/redfield/netctl/pkg/backend"
)

type mockFrontend struct {
	*frontend
}

func newMockFrontend() *mockFrontend {
	ctx, cancel := context.WithCancel(context.Background())

	f := &frontend{
		uuid:   uuid.New().String(),
		typ:    api.InterfaceType_UNSPECIFIED,
		ti:     api.NewIPv4Transport("", "50051"),
		init:   &initializer{},
		ctx:    ctx,
		cancel: cancel,
	}

	return &mockFrontend{f}
}

func (mf *mockFrontend) serve() error {
	mf.server = grpc.NewServer()

	lis, err := net.Listen(mf.ti.Network(), mf.ti.String())
	if err != nil {
		return errors.Wrap(err, "failed to create listener")
	}
	defer lis.Close()

	api.RegisterNetctlFrontServer(mf.server, mf)

	return mf.server.Serve(lis)
}

func (mf *mockFrontend) Notify(ctx context.Context, r *api.NotifyRequest) (*api.NotifyReply, error) {
	if err := mf.handleNotify(r.GetNotification()); err != nil {
		return nil, err
	}

	return &api.NotifyReply{}, nil
}

func (mf *mockFrontend) WirelessConnect(ctx context.Context, r *api.WirelessConnectRequest) (*api.WirelessConnectReply, error) {
	return nil, nil
}

func (mf *mockFrontend) WirelessDisconnect(ctx context.Context, r *api.WirelessDisconnectRequest) (*api.WirelessDisconnectReply, error) {
	return nil, nil
}

func (mf *mockFrontend) WirelessMonitorProperties(r *api.WirelessMonitorPropertiesRequest, stream api.NetctlFront_WirelessMonitorPropertiesServer) error {
	return nil
}

func (mf *mockFrontend) WirelessGetProperties(ctx context.Context, r *api.WirelessGetPropertiesRequest) (*api.WirelessGetPropertiesReply, error) {
	return nil, nil
}

func newTestBackend() (*backend.Backend, error) {
	return backend.NewBackend("tcp://:50052", backend.WithNoDatabase())
}

func backendTestMetaData(t *testing.T) *api.BackendMetaData {
	tr, err := api.ParseTransport("tcp://:50052")
	if err != nil {
		t.Fatalf("invalid backend metadata")
	}

	return &api.BackendMetaData{TransportInfo: tr.Info}
}

func backendTestClient(t *testing.T) *api.BackendClient {
	cc, err := grpc.Dial(":50052", grpc.WithInsecure())
	if err != nil {
		t.Fatalf("failed to create client conn: %v", err)
	}

	return api.NewBackendClient(cc)
}

func startServices() (*mockFrontend, *backend.Backend) {
	// Setup test backend
	b, err := newTestBackend()
	if err != nil {
		log.Fatalf("failed to start backend: %v", err)
	}

	go func() {
		if err := b.Serve(); err != nil {
			log.Print("backend down")
		}
	}()

	// Setup test frontend
	f := newMockFrontend()

	go func() {
		if err := f.serve(); err != nil {
			log.Print("frontend down")
		}
	}()

	return f, b
}

func TestInitializationAndTeardown(t *testing.T) {
	f, b := startServices()

	err := f.initialize(backendTestMetaData(t), 5*time.Second)
	if err != nil {
		t.Fatalf("initialization failed: %v", err)
	}

	err = f.teardown()
	if err != nil {
		t.Fatalf("teardown failed: %v", err)
	}

	err = b.Close()
	if err != nil {
		t.Fatalf("failed to close backend: %v", err)
	}
}

func TestRestartBackend(t *testing.T) {
	f, b := startServices()

	err := f.initialize(backendTestMetaData(t), 5*time.Second)
	if err != nil {
		t.Fatalf("initialization failed: %v", err)
	}

	// Restart the backend
	err = b.Close()
	if err != nil {
		t.Fatalf("failed to close backend: %v", err)
	}

	go func() {
		if err := b.Serve(); err != nil {
			log.Print("backend down")
		}
	}()

	// Confirm that the frontend has re-registered,
	// but give it some time.
	bc := backendTestClient(t)
	done := make(chan bool)

	go func() {
		for {
			_, err := bc.GetFrontend(f.uuid)
			if err != nil {
				time.Sleep(1 * time.Second)
				continue
			}

			done <- true
		}
	}()

	select {
	case <-time.After(10 * time.Second):
		t.Fatalf("frontend did not come back after 10 seconds")
	case <-done:
	}

	err = f.teardown()
	if err != nil {
		log.Printf("Failed to close frontend: %v", err)
	}
	err = b.Close()
	if err != nil {
		log.Printf("Failed to close backend: %v", err)
	}
}

func TestRestartFrontend(t *testing.T) {
	// Setup test backend
	b, err := newTestBackend()
	if err != nil {
		log.Fatalf("failed to start backend: %v", err)
	}

	go func() {
		if err := b.Serve(); err != nil {
			log.Print("backend down")
		}
	}()

	// Setup test frontend
	f := newMockFrontend()

	go func() {
		if err := f.serve(); err != nil {
			log.Print("frontend down")
		}
	}()

	err = f.initialize(backendTestMetaData(t), 5*time.Second)
	if err != nil {
		t.Fatalf("initialization failed: %v", err)
	}

	// Restart the frontend
	err = f.teardown()
	if err != nil {
		t.Fatalf("teardown failed: %v", err)
	}

	f = newMockFrontend()
	go func() {
		if err := f.serve(); err != nil {
			log.Printf("frontend down: %v", err)
		}
	}()

	err = f.initialize(backendTestMetaData(t), 5*time.Second)
	if err != nil {
		t.Fatalf("re-initialization failed: %v", err)
	}

	err = f.teardown()
	if err != nil {
		log.Printf("Failed to close frontend: %v", err)
	}
	err = b.Close()
	if err != nil {
		log.Printf("Failed to close backend: %v", err)
	}
}

func TestInitializationTimeout(t *testing.T) {
	// Create a TCP listener so that the client conn can be created,
	// but there is no real backend, so a timeout should be triggered
	// when the frontend tries to register.
	ln, err := net.Listen("tcp", "localhost:50052")
	if err != nil {
		t.Fatalf("Failed to start TCP listener: %v", err)
	}
	defer ln.Close()

	go func() {
		for {
			_, err := ln.Accept()
			if err != nil {
				return
			}
		}
	}()

	f := newMockFrontend()
	go func() {
		if err := f.serve(); err != nil {
			log.Print("frontend down")
		}
	}()

	err = f.initialize(backendTestMetaData(t), 1*time.Second)
	if err == nil {
		t.Fatalf("Expected timeout error in init phase, got nil!")
	}
}
