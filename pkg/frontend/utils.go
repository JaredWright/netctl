// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package frontend

import (
	"encoding/hex"
	"fmt"
	"io/ioutil"
	"strings"

	"github.com/google/uuid"
	"github.com/pkg/errors"

	"gitlab.com/redfield/netctl/api"
	"gitlab.com/redfield/netctl/pkg/wireless"
)

// hashInterfaceNameWithUUID will create a uuid from an md5 hash using
// the domain's uuid (from /sys/hypervisor/uuid), and the supplied
// network interface name.
func hashInterfaceNameWithUUID(ifname string) (string, error) {
	data, err := ioutil.ReadFile("/sys/hypervisor/uuid")
	if err != nil {
		return "", err
	}

	data = []byte(strings.ReplaceAll(string(data), "-", ""))
	data = []byte(strings.TrimSpace(string(data)))

	data, err = hex.DecodeString(string(data))
	if err != nil {
		return "", err
	}

	space, err := uuid.FromBytes(data)
	if err != nil {
		return "", err
	}

	hash := uuid.NewMD5(space, []byte(ifname))

	return hash.String(), nil
}

func formatNetworkConfiguration(conf *api.WirelessNetworkConfiguration) wireless.NetworkConfiguration {
	c := wireless.NetworkConfiguration{
		SSID:          conf.Ssid,
		PSK:           conf.Psk,
		KeyManagement: conf.KeyMgmt,
		Identity:      conf.Identity,
		Password:      conf.Password,
		EAP:           conf.Eap,
		CaCert:        conf.CaCert,
		ClientCert:    conf.ClientCert,
		PrivKey:       conf.PrivKey,
		PrivKeyPasswd: conf.PrivKeyPasswd,
		ScanSSID:      conf.ScanSsid,
	}

	return c
}

const (
	wirelessPCIClass = "0x028000"
	wiredPCIClass    = "0x020000"
)

// InterfaceType returns the type of network interface, specified by name. An error is returned
// if the interface does not exist, or is not of valid type.
func InterfaceType(ifname string) (api.InterfaceType, error) {
	class := interfacePCIClass(ifname)

	switch class {
	case wirelessPCIClass:
		return api.InterfaceType_WIRELESS, nil
	case wiredPCIClass:
		return api.InterfaceType_WIRED, nil
	}

	err := errors.Errorf("%v does not exist or is not of valid type", ifname)

	return api.InterfaceType_UNSPECIFIED, err
}

func interfacePCIClass(ifname string) string {
	path := "/sys/class/net/%v/device/class"

	b, err := ioutil.ReadFile(fmt.Sprintf(path, ifname))
	if err != nil {
		return ""
	}

	return strings.TrimSpace(string(b))
}

func convertWirelessState(state wireless.State) api.WirelessState {
	convertState := map[wireless.State]api.WirelessState{
		wireless.Disconnected: api.WirelessState_DISCONNECTED,
		wireless.Connected:    api.WirelessState_CONNECTED,
		wireless.Connecting:   api.WirelessState_CONNECTING,
		wireless.Failed:       api.WirelessState_FAILED,
	}

	return convertState[state]
}

func convertWirelessScanResults(results []wireless.ScanResult) []*api.AccessPoint {
	aps := make([]*api.AccessPoint, len(results))

	for i, result := range results {
		aps[i] = &api.AccessPoint{
			Ssid:           result.SSID,
			SignalStrength: result.SignalStrength.String(),
			KeyMgmt:        result.KeyManagement[0],
		}
	}

	return aps
}
