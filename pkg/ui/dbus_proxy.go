// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ui

import (
	"context"
	"io"
	"log"

	"github.com/godbus/dbus"
	"github.com/golang/protobuf/jsonpb"
	"github.com/pkg/errors"

	"gitlab.com/redfield/netctl/api"
)

const (
	dbusObjectPathWireless = "/com/gitlab/redfield/netctl/wireless"

	dbusDestinationWireless       = "com.gitlab.redfield.netctl.wireless"
	dbusWirelessPropertiesChanged = "com.gitlab.redfield.netctl.wireless.PropertiesChanged"
)

// DBusProxy is a UIProxy for a UI with DBus capabilities.
type DBusProxy struct {
	frontend     *api.FrontendClient
	frontendType api.InterfaceType
	marshaler    jsonpb.Marshaler
	done         chan bool
}

// NewDBusProxy returns a new dbus proxy.
func NewDBusProxy(ftype api.InterfaceType) *DBusProxy {
	marshaler := jsonpb.Marshaler{
		EnumsAsInts:  true,
		EmitDefaults: true,
		Indent:       "\t",
		OrigName:     true,
		AnyResolver:  nil,
	}

	dbp := &DBusProxy{
		marshaler:    marshaler,
		frontendType: ftype,
		done:         make(chan bool),
	}

	return dbp
}

func (dbp *DBusProxy) destination() string {
	if dbp.frontendType == api.InterfaceType_WIRELESS {
		return dbusDestinationWireless
	}

	return ""
}

func (dbp *DBusProxy) objectPath() dbus.ObjectPath {
	if dbp.frontendType == api.InterfaceType_WIRELESS {
		return dbus.ObjectPath(dbusObjectPathWireless)
	}

	return dbus.ObjectPath("")
}

// Serve starts the dbus proxy for the given frontend.
func (dbp *DBusProxy) Serve(frontend *api.FrontendClient) error {
	dbp.frontend = frontend
	defer dbp.frontend.Close()

	bus, err := dbus.SystemBus()
	if err != nil {
		return errors.Wrap(err, "failed to get system bus")
	}

	rn, err := bus.RequestName(dbp.destination(), dbus.NameFlagDoNotQueue)
	if err != nil {
		return errors.Wrap(err, "failed to get requested name on bus")
	}
	defer func() {
		_, err := bus.ReleaseName(dbp.destination())
		if err != nil {
			log.Printf("Failed to release name from system bus: %v", err)
		}
	}()

	// Make sure we own the name
	if rn != dbus.RequestNameReplyPrimaryOwner {
		return errors.Errorf("name %v is already in use on system bus", dbp.destination())
	}

	err = bus.Export(dbp, dbp.objectPath(), dbp.destination())
	if err != nil {
		return errors.Wrap(err, "failed to export DBus interface")
	}

	go func() {
		err := dbp.emitWirelessPropertiesChanged()
		if err != nil {
			log.Printf("Signal handler for %v failed: %v", dbusWirelessPropertiesChanged, err)
		}
	}()

	<-dbp.done

	return nil
}

// Close closes the dbus proxy.
func (dbp *DBusProxy) Close() {
	dbp.done <- true
}

func (dbp *DBusProxy) emitWirelessPropertiesChanged() error {
	bus, err := dbus.SystemBus()
	if err != nil {
		return errors.Wrap(err, "failed to get system bus")
	}

	stream, err := dbp.frontend.WirelessMonitorProperties(context.Background())
	if err != nil {
		return errors.Wrap(err, "failed to get wireless properties stream")
	}

	for {
		r, err := stream.Recv()
		if err != nil {
			if err == io.EOF {
				return nil
			}

			return errors.Wrap(err, "failed to receive wireless properties update")
		}

		update := r.GetUpdate()

		s, err := dbp.marshaler.MarshalToString(update)
		if err != nil {
			log.Printf("Failed to marshal update to JSON: %v", err)
			continue
		}

		err = bus.Emit(dbp.objectPath(), dbusWirelessPropertiesChanged, s)
		if err != nil {
			log.Printf("Faild to emit %v signal", dbusWirelessPropertiesChanged)
		}
	}
}

// WirelessConnect provides a DBus method to netctlfront.WirelessConnect.
func (dbp *DBusProxy) WirelessConnect(msg string) (int32, *dbus.Error) {
	var (
		conf  api.WirelessNetworkConfiguration
		state api.WirelessState
	)

	err := jsonpb.UnmarshalString(msg, &conf)

	if err != nil {
		err = errors.Wrap(err, "failed to unmarshal JSON string to network configuration")
		// Return error because as far as the DBus interface is concerned,
		// this is bad input, and the connection attempt never happens.
		return int32(state), dbus.MakeFailedError(err)
	}

	state, err = dbp.frontend.WirelessConnect(&conf)
	if err != nil {
		log.Printf("Failed to connect to wireless network: %v", err)
	}

	return int32(state), nil
}

// WirelessDisconnect provides a DBus method to netctlfront.WirelessDisconnect.
func (dbp *DBusProxy) WirelessDisconnect() *dbus.Error {
	err := dbp.frontend.WirelessDisconnect()
	if err != nil {
		return dbus.MakeFailedError(err)
	}

	return nil
}

// WirelessGetProperties provies a DBus method to netctlfront.WirelessGetProperties.
func (dbp *DBusProxy) WirelessGetProperties() (string, *dbus.Error) {
	props, err := dbp.frontend.WirelessGetProperties()
	if err != nil {
		return "", dbus.MakeFailedError(err)
	}

	s, err := dbp.marshaler.MarshalToString(props)
	if err != nil {
		return "", dbus.MakeFailedError(err)
	}

	return s, nil
}
