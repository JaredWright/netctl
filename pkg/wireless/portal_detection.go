// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package wireless

import (
	"context"
	"errors"
	"net"
	"net/http"
	"time"
)

// A well known URL that returns an empty page and status code 204
const noContentURL = "http://clients3.google.com/generate_204"

// HTTP client that is bound to a specified interface
func httpClientWithInterface(localIP net.IP) *http.Client {
	localTCPAddr := net.TCPAddr{
		IP: localIP,
	}

	resolver := &net.Resolver{
		PreferGo: true,
		Dial: func(ctx context.Context, network, addr string) (net.Conn, error) {
			d := &net.Dialer{}

			return d.DialContext(ctx, "udp", "8.8.8.8:53")
		},
	}

	dialer := net.Dialer{
		LocalAddr: &localTCPAddr,
		Timeout:   10 * time.Second,
		KeepAlive: 10 * time.Second,
		Resolver:  resolver,
	}

	transport := http.Transport{
		Proxy:               http.ProxyFromEnvironment,
		Dial:                dialer.Dial,
		TLSHandshakeTimeout: 10 * time.Second,
	}

	client := http.Client{
		Transport: &transport,
	}

	return &client
}

// CaptivePortalCheck tries to determine if a captive portal is present. When `err` is nil,
// `present` indicates if a captive portal is present. When possible, non-empty `portalURL`
// is returned to indicate the URL of the captive portal.
func (m *Manager) CaptivePortalCheck() (present bool, portalURL string, err error) {
	// Make a request to the well-known URL, we will expect to return an empty page
	// with status code 204
	ip, err := m.ip4Addr()
	if err != nil {
		return false, "", err
	}

	client := httpClientWithInterface(ip)

	// Try for up to 10 seconds
	for start := time.Now(); time.Since(start) < 10*time.Second; {
		response, err := client.Get(noContentURL)

		// If error is a timeout, return right away
		if err, ok := err.(net.Error); ok && err.Timeout() {
			// With a timeout, portal URL cannot be determined
			return true, "", err
		}

		if err != nil {
			// If error is not a timeout, try again
			continue
		}
		defer response.Body.Close()

		// If we make it here, we got a response - check the status code
		if response.StatusCode == http.StatusNoContent {
			// We got a 204 as expected - we are not behind a portal
			return false, "", nil
		}

		// If we receive any status other than 204 - assume we are behind a portal
		return true, response.Request.URL.String(), nil
	}

	// With a timeout, portal URL cannot be determined
	return true, "", errors.New("timeout: assuming captive portal")
}
