// Copyright 2020 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package wireless

import (
	"context"
	"fmt"
	"log"
	"sort"
	"strings"
	"sync"
	"time"

	"github.com/godbus/dbus"
)

// wpaProperties contains information we care about for tracking properties from
// various dbus interfaces exposed by wpa_supplicant.
type wpaProperties struct {
	sync.RWMutex

	// Track the current BSS. Use a context and cancel func to track the
	// BSS PropertiesChanged goroutine.
	currentBSS dbus.ObjectPath
	bssCtx     context.Context
	bssCancel  context.CancelFunc

	// Track the current network path.
	currentNetwork dbus.ObjectPath

	// Track "high-level" properties that we want to be easily available.
	ssid        string
	state       State
	signal      SignalStrength
	scanResults []ScanResult
}

// CurrentSSID returns the SSID of the current network, if there is one.
func (m *Manager) CurrentSSID() string {
	m.supplicant.props.RLock()
	defer m.supplicant.props.RUnlock()

	return m.supplicant.props.ssid
}

// State returns the current state of the interface managed by Manager.
func (m *Manager) State() State {
	m.supplicant.props.RLock()
	defer m.supplicant.props.RUnlock()

	return m.supplicant.props.state
}

// SignalStrength returns the signal strength of the current network, if
// associated.
func (m *Manager) SignalStrength() SignalStrength {
	m.supplicant.props.RLock()
	defer m.supplicant.props.RUnlock()

	return m.supplicant.props.signal
}

// AvailableNetworks returns the currently known networks from the latest successful
// AP scan. AP scans happen periodically, so ScanResults may change frequently.
func (m *Manager) AvailableNetworks() []ScanResult {
	m.supplicant.props.RLock()
	defer m.supplicant.props.RUnlock()

	results := make([]ScanResult, len(m.supplicant.props.scanResults))
	copy(results, m.supplicant.props.scanResults)

	return results
}

// getPropertiesChangedChannel registers a listener for the PropertiesChanged signal for the provided
// DBus interface.
func (s *wpaSupplicant) getPropertiesChangedChannel(path dbus.ObjectPath, intface string) (chan *dbus.Signal, error) {
	arg := "type='signal',path='%v',interface='%v',member='PropertiesChanged'"
	arg = fmt.Sprintf(arg, path, intface)

	call := s.bus.BusObject().Call("org.freedesktop.DBus.AddMatch", 0, arg)
	if err := call.Err; err != nil {
		return nil, err
	}

	// The channel should be sufficiently buffered so that signals
	// are not discarded.
	c := make(chan *dbus.Signal, 64)

	s.bus.Signal(c)

	return c, nil
}

func (s *wpaSupplicant) watchInterfacePropertiesChanged(ctx context.Context) {
	signal, err := s.getPropertiesChangedChannel(s.iface, "fi.w1.wpa_supplicant1.Interface")
	if err != nil {
		log.Printf("Failed to get PropertiesChanged channel: %v", err)
		return
	}
	defer s.bus.RemoveSignal(signal)

	// When this function exits, child goroutines e.g. the watchBSSPropertiesChanged
	// loop should also exit.
	defer func() {
		if s.props.bssCancel != nil {
			s.props.bssCancel()
		}
	}()

	for {
		var sig *dbus.Signal

		select {
		case <-ctx.Done():
			return
		case sig = <-signal:
		}

		props, ok := sig.Body[0].(map[string]dbus.Variant)
		if !ok {
			log.Printf("Got unexpected dbus signal body: %+v", sig.Body)
			continue
		}

		// This loop is responsible for the following properties:
		//
		//	- CurrentBSS
		//      - State
		//      - BSSs (scan results)
		//      - DisconnectReason (not tracked)
		//
		// If a property has changed, call it's corresponding handler.
		if state, ok := props["State"]; ok {
			s.handleInterfaceStateChange(state)
		}

		if bss, ok := props["CurrentBSS"]; ok {
			s.handleBSSChange(bss)
		}

		if results, ok := props["BSSs"]; ok {
			s.handleScanResultsChange(results)
		}

		if reason, ok := props["DisconnectReason"]; ok {
			s.handleDisconnectReason(reason)
		}
	}
}

func (s *wpaSupplicant) handleScanResultsChange(v dbus.Variant) {
	// The DBus property corresponds to a list of object paths
	// for each BSS found.
	paths, ok := v.Value().([]dbus.ObjectPath)
	if !ok {
		log.Print("Failed to handle scan results change: BSSs property unexpected type")
	}

	results := make(scanResults, 0)

	for _, bss := range paths {
		sr, err := s.newScanResultFromBSSObject(bss)
		if err != nil {
			log.Printf("Failed to get scan result information for %s: %v", bss, err)
			continue
		}

		results = append(results, sr)
	}

	// Make results are sorted by signal strength and are unique
	// with respect to SSID.
	sort.Sort(results)
	results = uniqueSSIDs(results)

	s.props.Lock()
	s.props.scanResults = results
	s.publisher.publish(ScanResultsChanged, s.props.scanResults)
	s.props.Unlock()
}

func (s *wpaSupplicant) handleInterfaceStateChange(v dbus.Variant) {
	// wpa_supplicant puts quotation marks around the state,
	// remove them
	wpaState := strings.Trim(v.Value().(string), `"`)

	// Just translate the wpa state into one of wireless.State, and
	// update if necessary.
	var state State

	switch wpaState {
	case wpaStateAuthenticating, wpaStateAssociating,
		wpaStateAssociated, wpaState4WayHandshake:

		state = Connecting

	case wpaStateDisconnected, wpaStateInactive:
		state = Disconnected

	case wpaStateCompleted:
		// Previously, the state handler would wait to assign Connected
		// until an IP address was assigned. It seems like this is unnecessary
		// for these purposes, so just directly translate 'completed' to Connected.
		state = Connected

	default:
		// Do not change the state, but log a message about to help determine
		// if the state should be tracked later.
		log.Printf("Ignoring WPA state change to %s", wpaState)

		return
	}

	s.props.Lock()
	if state != s.props.state {
		s.props.state = state
		s.publisher.publish(StateChanged, s.props.state)
	}
	s.props.Unlock()
}

func (s *wpaSupplicant) handleDisconnectReason(v dbus.Variant) {
	reason, ok := v.Value().(int32)
	if !ok {
		log.Printf("Cannot handle DisconnectReason: unexpected type %T", v.Value())
	}

	switch reason {
	case 2 /* PREV_AUTH_NOT_VALID (AP/remote) */ :
		// There is nothing to do here, handling this code is the job
		// of the association handler.
		log.Print("Got disconnect reason 2 (PREV_AUTH_NOT_VALID), not handling.")

		return

	case -3 /* DEAUTH_LEAVING (local) */ :
		// This usually means the user decided to disconnect. For now,
		// do nothing.
		log.Print("Got disconnect reason -3 (DEAUTH_LEAVING, local), not handling.")

		return

	case 3 /* DEAUTH_LEAVING (AP/remote) */ :
		// This happens, for example, when the station is connected to mobile hotspot
		// and it is turned off. The AP notifies connected stations that it is leaving.
		//
		// XXX: Start a routine to look for known networks.
		log.Print("Got disconnect reason 3 (DEAUTH_LEAVING, remote), not handling.")

		return

	case -4 /* STA_INACTIVE (local) */ :
		// This means the station has deemed the AP inactive. Usually this would be due
		// to the station physically moving away from the AP. However this can also happen
		// if beacons are lost for some other reason.
		log.Print("Got disconnect reason -4 (STA_INACTIVE, local). Attempting to re-connect...")

		// Do this in another goroutine so we don't block other handlers.
		go func() {
			ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
			defer cancel()

			status, err := s.reconnect(ctx)
			if err != nil {
				log.Printf("Failed to re-connect after inactivity: %v", err)
				return
			}

			// In general, this probably should not happen. If we get here, it means the err
			// above is nil, which suggests the association failed due to something like
			// bad auth data. This is unexpected when re-connecting after inactivity.
			if !status {
				log.Print("Failed to re-connect after inactivity: unknown reason")
				return
			}

			log.Print("Successfully re-connected after inactivity.")
		}()

	default:
		log.Printf("Got unhandled disconnect reason code %v.", reason)
	}
}

func (s *wpaSupplicant) handleBSSChange(v dbus.Variant) {
	bss, ok := v.Value().(dbus.ObjectPath)
	if !ok {
		log.Print("Cannot handle BSS change, property value is not expected type")
		return
	}

	// First, cancel the existing goroutine if there is one.
	if s.props.bssCancel != nil {
		s.props.bssCancel()
	}

	// Then, re-initialize the context.
	s.props.bssCtx, s.props.bssCancel = context.WithCancel(context.Background())

	// Initialize new BSS properties, possibly clearing them.
	if err := s.setInitialBSSProperties(bss); err != nil {
		log.Printf("Failed to set BSS properties: %v", err)
	}

	// Don't start the goroutine if we're not associated.
	if bss != "/" {
		go s.watchBSSPropertiesChanged(bss)
	}
}

func (s *wpaSupplicant) setInitialBSSProperties(bss dbus.ObjectPath) error {
	// If the bss path is "/", it means there is no associated BSS, and
	// everything should be cleared.
	if bss == "/" {
		s.props.Lock()

		s.props.currentBSS = bss
		s.props.ssid = ""
		s.publisher.publish(SSIDChanged, s.props.ssid)
		s.props.signal = 1
		s.publisher.publish(SignalStrengthChanged, s.props.signal)

		s.props.Unlock()

		return nil
	}

	sr, err := s.newScanResultFromBSSObject(bss)
	if err != nil {
		return err
	}

	s.props.Lock()

	s.props.currentBSS = bss
	s.props.ssid = sr.SSID
	s.publisher.publish(SSIDChanged, s.props.ssid)
	s.props.signal = sr.SignalStrength
	s.publisher.publish(SignalStrengthChanged, s.props.signal)

	s.props.Unlock()

	return nil
}

func (s *wpaSupplicant) watchBSSPropertiesChanged(bss dbus.ObjectPath) {
	signal, err := s.getPropertiesChangedChannel(bss, "fi.w1.wpa_supplicant1.BSS")
	if err != nil {
		log.Printf("Failed to get PropertiesChanged channel for BSS: %v", err)
		return
	}
	defer s.bus.RemoveSignal(signal)

	for {
		var sig *dbus.Signal

		select {
		case <-s.props.bssCtx.Done():
			return
		case sig = <-signal:
		}

		props, ok := sig.Body[0].(map[string]dbus.Variant)
		if !ok {
			log.Printf("Got unexpected dbus signal body: %+v", sig.Body)
			continue
		}

		if signal, ok := props["Signal"]; ok {
			s.handleSignalChange(signal)
		}
	}
}

func (s *wpaSupplicant) handleSignalChange(v dbus.Variant) {
	signal, ok := v.Value().(int16)
	if !ok {
		log.Print("Cannot handle BSS Signal change, property value is not expected type")
		return
	}

	s.props.Lock()
	s.props.signal = SignalStrength(signal)
	s.publisher.publish(SignalStrengthChanged, s.props.signal)
	s.props.Unlock()
}
