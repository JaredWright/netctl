// Copyright 2020 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package wireless

import (
	"context"
	"sync"

	"github.com/rs/xid"
)

// Notification is a notification for some wireless networking event.
// Examples include, scan results updated, connection state change,
// and signal strength change.
type Notification struct {
	Type  NotificationType
	Value interface{}
}

// NotificationType indicates the information given by the notification.
type NotificationType uint32

const (
	// StateChanged indicates that the connection state has changed.
	StateChanged NotificationType = iota + 1

	// SignalStrengthChanged indicates that the signal strength of the
	// associated AP (if any) has changed.
	SignalStrengthChanged

	// ScanResultsChanged indicates that the list of available networks/
	// scan results has changed.
	ScanResultsChanged

	// SSIDChanged indicates that the current network SSID has changed,
	// possibly to empty.
	SSIDChanged

	// IPChanged indicates that the IP address of the interface has changed.
	IPChanged

	// PortalDetected indicates that a captive portal has been detected.
	PortalDetected
)

// Notify will send all notification to c, and will stop when ctx is done.
func (m *Manager) Notify(ctx context.Context, c chan Notification) {
	sub := m.supplicant.publisher.subscribe()

	go func() {
		defer m.supplicant.publisher.unsubscribe(sub)

		for {
			select {
			case <-ctx.Done():
				return
			case c <- <-sub.update: // Send updates directly over.
			}
		}
	}()
}

// notificationPublisher receives updates over one channel, and distributes
// the update to all of its subscribers.
type notificationPublisher struct {
	update chan Notification

	mux         sync.Mutex
	subscribers map[string]chan Notification
}

// notificationSubscriber subscribes to a publisher and listens for updates.
type notificationSubscriber struct {
	id     string
	update chan Notification
}

func newNotificationPublisher() *notificationPublisher {
	p := &notificationPublisher{
		update:      make(chan Notification, 64),
		subscribers: make(map[string]chan Notification),
	}

	return p
}

func (p *notificationPublisher) subscribe() *notificationSubscriber {
	s := &notificationSubscriber{
		id:     xid.New().String(),
		update: make(chan Notification, 16),
	}

	p.mux.Lock()
	defer p.mux.Unlock()

	p.subscribers[s.id] = s.update

	return s
}

func (p *notificationPublisher) unsubscribe(s *notificationSubscriber) {
	p.mux.Lock()
	defer p.mux.Unlock()

	delete(p.subscribers, s.id)
}

func (p *notificationPublisher) publish(ntype NotificationType, value interface{}) {
	// Ensure that caller does not block.
	go func() {
		update := Notification{
			Type:  ntype,
			Value: value,
		}

		p.update <- update
	}()
}

func (p *notificationPublisher) serve(ctx context.Context) {
	for {
		select {
		case <-ctx.Done():
			p.mux.Lock()

			for _, s := range p.subscribers {
				close(s)
			}

			p.mux.Unlock()

			return

		case u := <-p.update:
			p.mux.Lock()

			for _, s := range p.subscribers {
				s <- u
			}

			p.mux.Unlock()
		}
	}
}
