// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package wireless

import (
	"context"
	"fmt"
	"log"
	"net"
	"strings"
	"time"

	"github.com/pkg/errors"
)

// State represents a connection state
type State int32

// Possible values of type State:
// 	Disconnected
// 	Connected
// 	Connecting
// 	Failed
const (
	Disconnected State = iota
	Connected
	Connecting
	Failed
)

// String returns the string represetation of the state.
func (s State) String() string {
	switch s {
	case Disconnected:
		return "disconnected"
	case Connected:
		return "connected"
	case Connecting:
		return "connecting"
	case Failed:
		return "failed"
	}

	return "unknown"
}

// Default scan intervals.
const (
	defaultConnectedScanInterval    = 90 * time.Second
	defaultDisconnectedScanInterval = 10 * time.Second
)

// Manager is used to manage a wireless network interface.
type Manager struct {
	supplicant *wpaSupplicant

	// Name of the wireless interface managed by Manager.
	iface string

	ctx    context.Context
	cancel context.CancelFunc

	opts *managerOptions
}

// NewManager returns a Manager for a specified wireless
// interface.
func NewManager(iface string, opts ...ManagerOption) (*Manager, error) {
	s, err := newSupplicant(iface)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create supplicant")
	}

	ctx, cancel := context.WithCancel(context.Background())

	m := Manager{
		supplicant: s,
		iface:      iface,
		ctx:        ctx,
		cancel:     cancel,
		opts:       defaultManagerOptions(),
	}

	for _, opt := range opts {
		opt.apply(m.opts)
	}

	go m.supplicant.publisher.serve(m.ctx)
	go m.supplicant.watchInterfacePropertiesChanged(m.ctx)
	go m.notifyIPChanges()

	if m.opts.autoPortalCheck {
		go m.notifyPortalDetected()
	}

	return &m, nil
}

// Close closes the Manager.
func (m *Manager) Close() error {
	if m.cancel != nil {
		m.cancel()
	}

	return nil
}

// ManagerOption is used to configure a Manager.
type ManagerOption interface {
	apply(*managerOptions)
}

type managerOptions struct {
	// These values are used to tell the manager how frequently
	// to perform AP scans.
	connectedScanInterval    time.Duration
	disconnectedScanInterval time.Duration

	// Automatically perform a portal check.
	autoPortalCheck bool
}

func defaultManagerOptions() *managerOptions {
	opts := &managerOptions{
		connectedScanInterval:    defaultConnectedScanInterval,
		disconnectedScanInterval: defaultDisconnectedScanInterval,
		autoPortalCheck:          true,
	}

	return opts
}

type funcManagerOption struct {
	f func(*managerOptions)
}

func (fmo *funcManagerOption) apply(m *managerOptions) {
	fmo.f(m)
}

func newFuncManagerOption(f func(*managerOptions)) *funcManagerOption {
	return &funcManagerOption{f}
}

// WithConnectedScanInterval sets the interval at which AP scans should
// be performed when the Manager's interface is in a Connected state.
func WithConnectedScanInterval(interval time.Duration) ManagerOption {
	return newFuncManagerOption(func(m *managerOptions) {
		m.connectedScanInterval = interval
	})
}

// WithDisconnectedScanInterval sets the interval at which AP scans should
// be performed when the Manager's interface is in a Disconnected state.
func WithDisconnectedScanInterval(interval time.Duration) ManagerOption {
	return newFuncManagerOption(func(m *managerOptions) {
		m.disconnectedScanInterval = interval
	})
}

// WithAutoPortalCheck enables/disables automatic captive portal checking when
// a new network is connected.
func WithAutoPortalCheck(enable bool) ManagerOption {
	return newFuncManagerOption(func(m *managerOptions) {
		m.autoPortalCheck = enable
	})
}

// ScanForNetworks begins scanning for available wireless networks, and does so until
// Manager is Close()'d, or the provided ctx is cancelled.
//
// The default behavior is for the manager to scan frequently while the interface is in
// a Disconnected state, and to scan less frequently when the interface state is Connected.
// These intervals can be set with the WithConnectedScanInterval and WithDisconnectedScanInterval
// ManagerOption's.
func (m *Manager) ScanForNetworks(ctx context.Context) {
	interval := 0 * time.Second

	for {
		// Using time.After instead of time.Sleep here ensures that if
		// a context is cancelled, the goroutine won't be stuck sleeping.
		select {
		case <-m.ctx.Done():
			return
		case <-ctx.Done():
			return
		case <-time.After(interval):
		}

		if err := m.supplicant.scan(); err != nil {
			log.Printf("Failed to perform scan from %s: %v", m.iface, err)
		}

		// Determine next sleep interval.
		interval = m.opts.disconnectedScanInterval

		if m.State() == Connected {
			interval = m.opts.connectedScanInterval
		}
	}
}

// ip4Addr returns the IPv4 address of the interface if it has one, and returns an error
// otherwise.
func (m *Manager) ip4Addr() (net.IP, error) {
	iface, err := net.InterfaceByName(m.iface)
	if err != nil {
		return net.IP{}, err
	}

	addrs, err := iface.Addrs()
	if err != nil {
		return net.IP{}, err
	}

	// Look for valid IP address
	for _, v := range addrs {
		// Make sure the address is valid IPv4 in dotted decimal notation
		addr := strings.Split(v.String(), "/")[0]
		ip := net.ParseIP(addr).To4()

		if ip != nil {
			// Found a valid IPv4 address
			return ip, nil
		}
	}

	// Interface has no addresses
	return net.IP{}, fmt.Errorf("%v has no valid IPv4 address", m.iface)
}

// Connect connects to a network using a specified configuration.
func (m *Manager) Connect(conf NetworkConfiguration) (State, error) {
	_ = m.supplicant.removeConfiguredNetwork()

	// Timeout's sometimes can be annoying during a connection attempt. For now,
	// use context.Background _unless_ ScanSSID is set. In this case, we should
	// set a reasonable timeout incase the SSID is not found.
	ctx := context.Background()

	if conf.ScanSSID != 0 {
		ctxTimeout, cancel := context.WithTimeout(context.Background(), 20*time.Second)
		defer cancel()

		ctx = ctxTimeout
	}

	success, err := m.supplicant.connect(ctx, conf)
	if !success {
		_ = m.supplicant.removeConfiguredNetwork()
		// err might be nil, which would indicate bad auth data.
		return Failed, err
	}

	return Connected, nil
}

// Disconnect disconnects the interface from the current network.
func (m *Manager) Disconnect() error {
	if m.State() == Disconnected {
		return nil
	}

	return m.supplicant.removeConfiguredNetwork()
}

// IPAddress returns a dotted decimal string representation of
// the Interface's IP address. If it has no IP, an empty string
// is returned.
func (m *Manager) IPAddress() string {
	ip, err := m.ip4Addr()
	if err != nil {
		return ""
	}

	return ip.String()
}

// notifyIPChanges polls the interface's IP address and publishes
// updates for callers of Notify.
func (m *Manager) notifyIPChanges() {
	// Just do a simple poll.
	var oldIP string

	for {
		select {
		case <-m.ctx.Done():
			return
		case <-time.After(1 * time.Second):
		}

		if ip := m.IPAddress(); ip != oldIP {
			m.supplicant.publisher.publish(IPChanged, ip)
			oldIP = ip
		}
	}
}

func (m *Manager) notifyPortalDetected() {
	sub := m.supplicant.publisher.subscribe()
	defer m.supplicant.publisher.unsubscribe(sub)

	for {
		var n Notification

		select {
		case <-m.ctx.Done():
			return
		case n = <-sub.update:
		}

		// When the state changes to Connected, check for a captive portal. Ignore
		// all other notifications.
		if n.Type != StateChanged {
			continue
		}

		if n.Value.(State) != Connected {
			continue
		}

		// Try up to 5 times to get a reliable portal check, e.g. err == nil.
		var (
			pres bool
			url  string
			err  error
		)

		for i := 0; i < 5; i++ {
			pres, url, err = m.CaptivePortalCheck()
			if err == nil {
				break
			}

			log.Printf("Failed to perform captive portal check: %v (try %v/5)", err, i+1)

			// If the interface does not have an IP address, the portal check will
			// fail immediately. Increase the sleep if there is no IP.
			sleep := 1 * time.Second
			if m.IPAddress() == "" {
				sleep = 3 * time.Second
			}

			select {
			case <-m.ctx.Done():
				return
			case <-time.After(sleep):
			}
		}

		// If err is still non-nil, give up and continue on in the outer loop.
		if err != nil {
			continue
		}

		if pres {
			m.supplicant.publisher.publish(PortalDetected, url)
		}
	}
}
