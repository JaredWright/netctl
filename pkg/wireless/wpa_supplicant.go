// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package wireless

import (
	"context"
	"log"
	"reflect"
	"sync"

	"github.com/godbus/dbus"
	"github.com/pkg/errors"
)

const (
	// DBus destination and base object path for wpa_supplicant.
	wpaDBusDestination = "fi.w1.wpa_supplicant1"
	wpaBaseObjectPath  = "/fi/w1/wpa_supplicant1"
)

// wpa_supplicant interface states
const (
	wpaStateDisconnected = "disconnected"
	wpaStateInactive     = "inactive"
	//wpaStateScanning       = "scanning"
	wpaStateAuthenticating = "authenticating"
	wpaStateAssociating    = "associating"
	wpaStateAssociated     = "associated"
	wpaState4WayHandshake  = "4way_handshake"
	//wpaStateGroupHandshake = "group_handshake"
	wpaStateCompleted = "completed"
	//wpaStateUnknown   = "unknown"
)

// wpa_supplicant key management options
const (
	AuthOpen = "NONE"
	AuthPSK  = "WPA-PSK"
)

// NetworkConfiguration represents a network configuration as described
// in wpa_supplicant.conf(5)
type NetworkConfiguration struct {
	SSID          string `conf:"ssid"`
	PSK           string `conf:"psk"`
	KeyManagement string `conf:"key_mgmt"`
	Identity      string `conf:"identity"`
	Password      string `conf:"password"`
	EAP           string `conf:"eap"`
	CaCert        string `conf:"ca_cert"`
	ClientCert    string `conf:"client_cert"`
	PrivKey       string `conf:"private_key"`
	PrivKeyPasswd string `conf:"private_key_passwd"`
	ScanSSID      int32  `conf:"scan_ssid"`
}

// Create config map needed by DBus out of NetworkConfiguration type
func networkConfigurationAsMap(c NetworkConfiguration) map[string]interface{} {
	config := make(map[string]interface{})

	// Use reflect to get fields values and keys
	t := reflect.TypeOf(c)
	for i := 0; i < t.NumField(); i++ {
		// Use the field tag to get map key
		key := t.Field(i).Tag.Get("conf")

		// Get the value of the field
		v := reflect.ValueOf(&c).Elem()
		value := v.Field(i).Interface()

		// Only set in the config if 'value' is not its type's zero value
		if value != reflect.Zero(reflect.TypeOf(value)).Interface() {
			config[key] = value
		}
	}

	return config
}

// wpaSupplicant provides a mechanism to interface with
// the wpa_supplicant DBus API.
type wpaSupplicant struct {
	bus *dbus.Conn // DBus connection

	iface dbus.ObjectPath // Object path for network interface

	ac        *associationContext
	props     *wpaProperties
	publisher *notificationPublisher
}

// newSupplicant returns a Supplicant associated with a wireless interface
// specified by ifname.
func newSupplicant(ifname string) (*wpaSupplicant, error) {
	// Connect to system bus
	bus, err := dbus.SystemBus()
	if err != nil {
		return nil, err
	}

	var iface dbus.ObjectPath

	// Create a bus object to the base path.
	obj := bus.Object(wpaDBusDestination, wpaBaseObjectPath)

	err = obj.Call("fi.w1.wpa_supplicant1.GetInterface", 0, ifname).Store(&iface)
	if err != nil {
		return nil, err
	}

	s := wpaSupplicant{
		bus:   bus,
		iface: iface,
		ac:    &associationContext{},
		props: &wpaProperties{
			currentNetwork: "/",
			state:          Disconnected,
		},
		publisher: newNotificationPublisher(),
	}

	return &s, nil
}

// associationContext is a convenience type for controlling association attempts.
// This type helps ensure that only one goroutine is actively making an association
// attempt, but that such goroutines can be preempted, for example by a newer goroutine
// that needs to make an association attempt.
type associationContext struct {
	sync.Mutex            // association lock
	pmux       sync.Mutex // preemption lock

	done chan struct{}
}

func (ac *associationContext) preempt() {
	// Get the preemption lock.
	ac.pmux.Lock()

	// Preempt the existing goroutine, if necessary.
	if ac.done != nil {
		close(ac.done)
	}

	// Now that the context has been cancelled, the
	// active goroutine should release the association
	// lock.
	//
	// Unlock when association is done!
	ac.Lock()

	// Now that we're here, we know there is not a goroutune using
	// this chan, so it is safe to re-initialize.
	ac.done = make(chan struct{})

	// Now that we have the association lock and have re-intialized
	// the context, we can release the preemption lock.
	ac.pmux.Unlock()
}

type associationPreempted struct{ error }

// waitForAssociationResult watches PropertiesChanged for State and DisconnectReason in order
// to determine the result of a connection attempt. An error is returned if there was an error
// with the DBus interface. Otherwise, a nil error and bool are returned, where the bool represents
// the success of the connection attempt.
func (s *wpaSupplicant) waitForAssociationResult(ctx context.Context) (bool, error) {
	signal, err := s.getPropertiesChangedChannel(s.iface, "fi.w1.wpa_supplicant1.Interface")
	if err != nil {
		return false, err
	}
	defer s.bus.RemoveSignal(signal)

	// When we try to connect to a network, it should ultimately result in either:
	//
	//   (1) The interface state changes to "completed";
	//   (2) Some disconnect reason is set; or
	//   (3) Some non-zero auth status code is set.
	//
	// (2) will most likely occur in the case of bad auth data, i.e. reason code 2
	// (PREV_AUTH_INVALID). (3) just should not happen as long as WEP Shared Key auth
	// is not being used (which is the norm). Handle (3) by reporting the error so
	// this loop will terminate.
	for {
		var sig *dbus.Signal

		select {
		case <-ctx.Done():
			return false, ctx.Err()

		case <-s.ac.done:
			return false, associationPreempted{errors.New("association preempted")}

		case sig = <-signal:
			// Check the signal
		}

		props, ok := sig.Body[0].(map[string]dbus.Variant)
		if !ok {
			return false, errors.New("unexpected signal body")
		}

		if v, ok := props["State"]; ok {
			state, ok := v.Value().(string)
			if !ok {
				log.Printf("Failed to read state: got unexpected type %T", v.Value())
				continue
			}

			if state == wpaStateCompleted {
				return true, nil
			}
		}

		if v, ok := props["DisconnectReason"]; ok {
			reason, ok := v.Value().(int32)
			if !ok {
				log.Printf("Failed to read disconnect reason: got unexpected type %T", v.Value())
				continue
			}

			if reason == 2 /* PREV_AUTH_NOT_VALID */ {
				return false, nil
			}

			return false, errors.Errorf("got unexpected disconnected reason code: %v", reason)
		}

		if v, ok := props["AuthStatusCode"]; ok {
			status, ok := v.Value().(int32)
			if !ok {
				log.Printf("Failed to read auth status code: got unexpected type %T", v.Value())
				continue
			}

			if status != 0 /* SUCCESS */ {
				return false, errors.Errorf("got non-zero auth status code: %v", status)
			}
		}
	}
}

func (s *wpaSupplicant) connect(ctx context.Context, cfg NetworkConfiguration) (bool, error) {
	// Get the association lock.
	s.ac.preempt()
	defer s.ac.Unlock()

	obj := s.bus.Object(wpaDBusDestination, s.iface)

	// Make the call to AddNetwork, and store the result path.
	var path dbus.ObjectPath

	// Make the configuration a map so that it can be used as args
	// to the DBus call.
	args := networkConfigurationAsMap(cfg)

	err := obj.Call("fi.w1.wpa_supplicant1.Interface.AddNetwork", 0, args).Store(&path)
	if err != nil {
		return false, errors.Wrap(err, "failed to add network configuration")
	}

	// Now select the network configuration we just added.
	call := obj.Call("fi.w1.wpa_supplicant1.Interface.SelectNetwork", 0, path)
	if call.Err != nil {
		return false, call.Err
	}

	// Since the configuration and selection was successful, save
	// the path to the current network.
	s.props.Lock()
	s.props.currentNetwork = path
	s.props.Unlock()

	// Now wait for the association result.
	return s.waitForAssociationResult(ctx)
}

func (s *wpaSupplicant) reconnect(ctx context.Context) (bool, error) {
	// Get the association lock.
	s.ac.preempt()
	defer s.ac.Unlock()

	obj := s.bus.Object(wpaDBusDestination, s.iface)

	call := obj.Call("fi.w1.wpa_supplicant1.Interface.Reconnect", 0)
	if call.Err != nil {
		return false, call.Err
	}

	return s.waitForAssociationResult(ctx)
}

// removeConfiguredNetwork removes (and disconnects) the currently configured
// network. If there is no network, nothing happens and nil error is returned.
func (s *wpaSupplicant) removeConfiguredNetwork() error {
	s.props.Lock()
	defer s.props.Unlock()

	// Nothing to do!
	if s.props.currentNetwork == "/" {
		return nil
	}

	obj := s.bus.Object(wpaDBusDestination, s.iface)

	call := obj.Call("fi.w1.wpa_supplicant1.Interface.RemoveNetwork", 0, s.props.currentNetwork)

	if call.Err != nil {
		return call.Err
	}

	s.props.currentNetwork = "/"

	return nil
}

// scan performs an AP scan.
func (s *wpaSupplicant) scan() error {
	// Args for wpa_supplicant scan
	args := make(map[string]interface{})

	args["Type"] = "active"
	args["AllowRoam"] = false

	obj := s.bus.Object(wpaDBusDestination, s.iface)
	c := obj.Call("fi.w1.wpa_supplicant1.Interface.Scan", 0, args)

	// If all goes well this will return nil
	return c.Err
}

// InterfaceNames returns a list of wireless interface names known to
// wpa_supplicant.
func InterfaceNames() ([]string, error) {
	names := make([]string, 0)

	bus, err := dbus.SystemBus()
	if err != nil {
		return names, err
	}

	obj := bus.Object(wpaDBusDestination, wpaBaseObjectPath)
	result, err := obj.GetProperty("fi.w1.wpa_supplicant1.Interfaces")
	if err != nil {
		return names, err
	}

	ifaces := result.Value().([]dbus.ObjectPath)
	for _, v := range ifaces {
		// v is an object path to an interface
		obj = bus.Object(wpaDBusDestination, v)
		result, err = obj.GetProperty("fi.w1.wpa_supplicant1.Interface.Ifname")
		if err != nil {
			// Continue to next interface
			continue
		}
		names = append(names, result.Value().(string))
	}

	return names, nil
}
