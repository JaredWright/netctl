// Copyright 2020 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package wireless

import (
	"bytes"

	"github.com/godbus/dbus"
	"github.com/pkg/errors"
)

// SignalStrength represents the strength of a signal in -dBm.
type SignalStrength int16

// String returns a string description of SignalStrength.
func (s SignalStrength) String() string {
	// Signal strength is represented in -dBm (0 to -100)
	// The closer to 0, the better the signal
	switch {
	case s > 0:
		// BSSSignalStrength returns 1 on error
		return ""
	case s >= -30:
		return "Excellent"
	case s >= -67:
		return "Very Good"
	case s >= -70:
		return "Fair"
	case s >= -80:
		return "Weak"
	case s >= -90:
		return "Very Weak"
	}

	return "Unknown"
}

// ScanResult contains information about an available AP.
type ScanResult struct {
	SSID           string         // Name of the network
	KeyManagement  []string       // Key management suite
	SignalStrength SignalStrength // Signal strength of the BSS
}

// newScanResultFromDBusObject returns a ScanResult according to the data given
// by the dbus object at bss, which must be a fi.w1.wpa_supplicant1.BSS.
func (s *wpaSupplicant) newScanResultFromBSSObject(bss dbus.ObjectPath) (ScanResult, error) {
	var sr ScanResult

	obj := s.bus.Object(wpaDBusDestination, bss)

	ssid, err := getSSIDFromBSSObject(obj)
	if err != nil {
		return sr, err
	}

	signal, err := getSignalFromBSSObject(obj)
	if err != nil {
		return sr, err
	}

	mgmt, err := getKeyMgmtFromBSSObject(obj)
	if err != nil {
		return sr, err
	}

	sr = ScanResult{
		SSID:           ssid,
		SignalStrength: signal,
		KeyManagement:  mgmt,
	}

	return sr, nil
}

func getSSIDFromBSSObject(obj dbus.BusObject) (string, error) {
	// Get the SSID property from the BSS.
	v, err := obj.GetProperty("fi.w1.wpa_supplicant1.BSS.SSID")
	if err != nil {
		return "", errors.Wrap(err, "could not get SSID property")
	}

	ssid, ok := v.Value().([]byte)
	if !ok {
		return "", errors.New("got unexpected type for SSID")
	}

	// Sometimes a null byte array is given, which causes issues
	// down the line. We are going to assume this is meant to
	// represent a hidden network, so we will replace this with
	// an empty string.
	if bytes.Equal(ssid, make([]byte, len(ssid))) {
		ssid = []byte("")
	}

	return string(ssid), nil
}

func getSignalFromBSSObject(obj dbus.BusObject) (SignalStrength, error) {
	// Get the Signal property from the BSS.
	v, err := obj.GetProperty("fi.w1.wpa_supplicant1.BSS.Signal")
	if err != nil {
		return 1, errors.Wrap(err, "could not get Signal property")
	}

	signal, ok := v.Value().(int16)
	if !ok {
		return 1, errors.New("got unexpected type for Signal")
	}

	return SignalStrength(signal), nil
}

func getKeyMgmtFromBSSObject(obj dbus.BusObject) ([]string, error) {
	// Get the key management suite
	v, err := obj.GetProperty("fi.w1.wpa_supplicant1.BSS.RSN")
	if err != nil {
		return nil, errors.Wrap(err, "could not get RSN property")
	}

	rsn, ok := v.Value().(map[string]dbus.Variant)
	if !ok {
		return nil, errors.New("got unexpected type for RSN")
	}

	v, ok = rsn["KeyMgmt"]
	if !ok {
		return nil, errors.New("no KeyMgmt property for RSN")
	}

	km, ok := v.Value().([]string)
	if !ok {
		return nil, errors.New("got unexpected type for RSN KeyMgmt")
	}

	if len(km) == 0 {
		km = append(km, "unknown")
	}

	return km, nil
}

// scanResults is used to implement sort.Interface, so that scan
// results can be sorted by SignalStrength.
type scanResults []ScanResult

func (sr scanResults) Len() int {
	return len(sr)
}

func (sr scanResults) Less(i, j int) bool {
	// A higher number, i.e. closer to zero since SignalStrength
	// should be in -dBm, represents a better signal, and therefore
	// should be sorted first.
	//
	// However, if the signal strength is positive, an error occurred
	// and that should be sorted last.
	if sr[i].SignalStrength > 0 {
		return false
	}

	if sr[j].SignalStrength > 0 {
		return true
	}

	return (sr[i].SignalStrength >= sr[j].SignalStrength)
}

func (sr scanResults) Swap(i, j int) {
	sr[i], sr[j] = sr[j], sr[i]
}

// Remove duplicate SSIDs from list
func uniqueSSIDs(results scanResults) scanResults {
	keys := make(map[string]bool)
	list := make(scanResults, 0)

	for _, r := range results {
		if _, exists := keys[r.SSID]; !exists {
			keys[r.SSID] = true
			list = append(list, r)
		}
	}

	return list
}
