// GENERATED CODE -- DO NOT EDIT!

'use strict';
var grpc = require('grpc');
var netctl_pb = require('./netctl_pb.js');

function serialize_api_GetAllFrontendsReply(arg) {
  if (!(arg instanceof netctl_pb.GetAllFrontendsReply)) {
    throw new Error('Expected argument of type api.GetAllFrontendsReply');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_api_GetAllFrontendsReply(buffer_arg) {
  return netctl_pb.GetAllFrontendsReply.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_api_GetAllFrontendsRequest(arg) {
  if (!(arg instanceof netctl_pb.GetAllFrontendsRequest)) {
    throw new Error('Expected argument of type api.GetAllFrontendsRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_api_GetAllFrontendsRequest(buffer_arg) {
  return netctl_pb.GetAllFrontendsRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_api_GetFrontendReply(arg) {
  if (!(arg instanceof netctl_pb.GetFrontendReply)) {
    throw new Error('Expected argument of type api.GetFrontendReply');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_api_GetFrontendReply(buffer_arg) {
  return netctl_pb.GetFrontendReply.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_api_GetFrontendRequest(arg) {
  if (!(arg instanceof netctl_pb.GetFrontendRequest)) {
    throw new Error('Expected argument of type api.GetFrontendRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_api_GetFrontendRequest(buffer_arg) {
  return netctl_pb.GetFrontendRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_api_GetSavedNetworksReply(arg) {
  if (!(arg instanceof netctl_pb.GetSavedNetworksReply)) {
    throw new Error('Expected argument of type api.GetSavedNetworksReply');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_api_GetSavedNetworksReply(buffer_arg) {
  return netctl_pb.GetSavedNetworksReply.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_api_GetSavedNetworksRequest(arg) {
  if (!(arg instanceof netctl_pb.GetSavedNetworksRequest)) {
    throw new Error('Expected argument of type api.GetSavedNetworksRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_api_GetSavedNetworksRequest(buffer_arg) {
  return netctl_pb.GetSavedNetworksRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_api_NotifyReply(arg) {
  if (!(arg instanceof netctl_pb.NotifyReply)) {
    throw new Error('Expected argument of type api.NotifyReply');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_api_NotifyReply(buffer_arg) {
  return netctl_pb.NotifyReply.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_api_NotifyRequest(arg) {
  if (!(arg instanceof netctl_pb.NotifyRequest)) {
    throw new Error('Expected argument of type api.NotifyRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_api_NotifyRequest(buffer_arg) {
  return netctl_pb.NotifyRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_api_RegisterFrontendReply(arg) {
  if (!(arg instanceof netctl_pb.RegisterFrontendReply)) {
    throw new Error('Expected argument of type api.RegisterFrontendReply');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_api_RegisterFrontendReply(buffer_arg) {
  return netctl_pb.RegisterFrontendReply.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_api_RegisterFrontendRequest(arg) {
  if (!(arg instanceof netctl_pb.RegisterFrontendRequest)) {
    throw new Error('Expected argument of type api.RegisterFrontendRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_api_RegisterFrontendRequest(buffer_arg) {
  return netctl_pb.RegisterFrontendRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_api_SaveNetworkReply(arg) {
  if (!(arg instanceof netctl_pb.SaveNetworkReply)) {
    throw new Error('Expected argument of type api.SaveNetworkReply');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_api_SaveNetworkReply(buffer_arg) {
  return netctl_pb.SaveNetworkReply.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_api_SaveNetworkRequest(arg) {
  if (!(arg instanceof netctl_pb.SaveNetworkRequest)) {
    throw new Error('Expected argument of type api.SaveNetworkRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_api_SaveNetworkRequest(buffer_arg) {
  return netctl_pb.SaveNetworkRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_api_UnregisterFrontendReply(arg) {
  if (!(arg instanceof netctl_pb.UnregisterFrontendReply)) {
    throw new Error('Expected argument of type api.UnregisterFrontendReply');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_api_UnregisterFrontendReply(buffer_arg) {
  return netctl_pb.UnregisterFrontendReply.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_api_UnregisterFrontendRequest(arg) {
  if (!(arg instanceof netctl_pb.UnregisterFrontendRequest)) {
    throw new Error('Expected argument of type api.UnregisterFrontendRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_api_UnregisterFrontendRequest(buffer_arg) {
  return netctl_pb.UnregisterFrontendRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_api_WirelessConnectReply(arg) {
  if (!(arg instanceof netctl_pb.WirelessConnectReply)) {
    throw new Error('Expected argument of type api.WirelessConnectReply');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_api_WirelessConnectReply(buffer_arg) {
  return netctl_pb.WirelessConnectReply.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_api_WirelessConnectRequest(arg) {
  if (!(arg instanceof netctl_pb.WirelessConnectRequest)) {
    throw new Error('Expected argument of type api.WirelessConnectRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_api_WirelessConnectRequest(buffer_arg) {
  return netctl_pb.WirelessConnectRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_api_WirelessDisconnectReply(arg) {
  if (!(arg instanceof netctl_pb.WirelessDisconnectReply)) {
    throw new Error('Expected argument of type api.WirelessDisconnectReply');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_api_WirelessDisconnectReply(buffer_arg) {
  return netctl_pb.WirelessDisconnectReply.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_api_WirelessDisconnectRequest(arg) {
  if (!(arg instanceof netctl_pb.WirelessDisconnectRequest)) {
    throw new Error('Expected argument of type api.WirelessDisconnectRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_api_WirelessDisconnectRequest(buffer_arg) {
  return netctl_pb.WirelessDisconnectRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_api_WirelessGetPropertiesReply(arg) {
  if (!(arg instanceof netctl_pb.WirelessGetPropertiesReply)) {
    throw new Error('Expected argument of type api.WirelessGetPropertiesReply');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_api_WirelessGetPropertiesReply(buffer_arg) {
  return netctl_pb.WirelessGetPropertiesReply.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_api_WirelessGetPropertiesRequest(arg) {
  if (!(arg instanceof netctl_pb.WirelessGetPropertiesRequest)) {
    throw new Error('Expected argument of type api.WirelessGetPropertiesRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_api_WirelessGetPropertiesRequest(buffer_arg) {
  return netctl_pb.WirelessGetPropertiesRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_api_WirelessMonitorPropertiesReply(arg) {
  if (!(arg instanceof netctl_pb.WirelessMonitorPropertiesReply)) {
    throw new Error('Expected argument of type api.WirelessMonitorPropertiesReply');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_api_WirelessMonitorPropertiesReply(buffer_arg) {
  return netctl_pb.WirelessMonitorPropertiesReply.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_api_WirelessMonitorPropertiesRequest(arg) {
  if (!(arg instanceof netctl_pb.WirelessMonitorPropertiesRequest)) {
    throw new Error('Expected argument of type api.WirelessMonitorPropertiesRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_api_WirelessMonitorPropertiesRequest(buffer_arg) {
  return netctl_pb.WirelessMonitorPropertiesRequest.deserializeBinary(new Uint8Array(buffer_arg));
}


var NetctlBackService = exports.NetctlBackService = {
  registerFrontend: {
    path: '/api.NetctlBack/RegisterFrontend',
    requestStream: false,
    responseStream: false,
    requestType: netctl_pb.RegisterFrontendRequest,
    responseType: netctl_pb.RegisterFrontendReply,
    requestSerialize: serialize_api_RegisterFrontendRequest,
    requestDeserialize: deserialize_api_RegisterFrontendRequest,
    responseSerialize: serialize_api_RegisterFrontendReply,
    responseDeserialize: deserialize_api_RegisterFrontendReply,
  },
  unregisterFrontend: {
    path: '/api.NetctlBack/UnregisterFrontend',
    requestStream: false,
    responseStream: false,
    requestType: netctl_pb.UnregisterFrontendRequest,
    responseType: netctl_pb.UnregisterFrontendReply,
    requestSerialize: serialize_api_UnregisterFrontendRequest,
    requestDeserialize: deserialize_api_UnregisterFrontendRequest,
    responseSerialize: serialize_api_UnregisterFrontendReply,
    responseDeserialize: deserialize_api_UnregisterFrontendReply,
  },
  getAllFrontends: {
    path: '/api.NetctlBack/GetAllFrontends',
    requestStream: false,
    responseStream: false,
    requestType: netctl_pb.GetAllFrontendsRequest,
    responseType: netctl_pb.GetAllFrontendsReply,
    requestSerialize: serialize_api_GetAllFrontendsRequest,
    requestDeserialize: deserialize_api_GetAllFrontendsRequest,
    responseSerialize: serialize_api_GetAllFrontendsReply,
    responseDeserialize: deserialize_api_GetAllFrontendsReply,
  },
  getFrontend: {
    path: '/api.NetctlBack/GetFrontend',
    requestStream: false,
    responseStream: false,
    requestType: netctl_pb.GetFrontendRequest,
    responseType: netctl_pb.GetFrontendReply,
    requestSerialize: serialize_api_GetFrontendRequest,
    requestDeserialize: deserialize_api_GetFrontendRequest,
    responseSerialize: serialize_api_GetFrontendReply,
    responseDeserialize: deserialize_api_GetFrontendReply,
  },
  // Storage-accessing APIs
getSavedNetworks: {
    path: '/api.NetctlBack/GetSavedNetworks',
    requestStream: false,
    responseStream: false,
    requestType: netctl_pb.GetSavedNetworksRequest,
    responseType: netctl_pb.GetSavedNetworksReply,
    requestSerialize: serialize_api_GetSavedNetworksRequest,
    requestDeserialize: deserialize_api_GetSavedNetworksRequest,
    responseSerialize: serialize_api_GetSavedNetworksReply,
    responseDeserialize: deserialize_api_GetSavedNetworksReply,
  },
  saveNetwork: {
    path: '/api.NetctlBack/SaveNetwork',
    requestStream: false,
    responseStream: false,
    requestType: netctl_pb.SaveNetworkRequest,
    responseType: netctl_pb.SaveNetworkReply,
    requestSerialize: serialize_api_SaveNetworkRequest,
    requestDeserialize: deserialize_api_SaveNetworkRequest,
    responseSerialize: serialize_api_SaveNetworkReply,
    responseDeserialize: deserialize_api_SaveNetworkReply,
  },
};

exports.NetctlBackClient = grpc.makeGenericClientConstructor(NetctlBackService);
var NetctlFrontService = exports.NetctlFrontService = {
  notify: {
    path: '/api.NetctlFront/Notify',
    requestStream: false,
    responseStream: false,
    requestType: netctl_pb.NotifyRequest,
    responseType: netctl_pb.NotifyReply,
    requestSerialize: serialize_api_NotifyRequest,
    requestDeserialize: deserialize_api_NotifyRequest,
    responseSerialize: serialize_api_NotifyReply,
    responseDeserialize: deserialize_api_NotifyReply,
  },
  // Wireless networking APIs
wirelessMonitorProperties: {
    path: '/api.NetctlFront/WirelessMonitorProperties',
    requestStream: false,
    responseStream: true,
    requestType: netctl_pb.WirelessMonitorPropertiesRequest,
    responseType: netctl_pb.WirelessMonitorPropertiesReply,
    requestSerialize: serialize_api_WirelessMonitorPropertiesRequest,
    requestDeserialize: deserialize_api_WirelessMonitorPropertiesRequest,
    responseSerialize: serialize_api_WirelessMonitorPropertiesReply,
    responseDeserialize: deserialize_api_WirelessMonitorPropertiesReply,
  },
  wirelessGetProperties: {
    path: '/api.NetctlFront/WirelessGetProperties',
    requestStream: false,
    responseStream: false,
    requestType: netctl_pb.WirelessGetPropertiesRequest,
    responseType: netctl_pb.WirelessGetPropertiesReply,
    requestSerialize: serialize_api_WirelessGetPropertiesRequest,
    requestDeserialize: deserialize_api_WirelessGetPropertiesRequest,
    responseSerialize: serialize_api_WirelessGetPropertiesReply,
    responseDeserialize: deserialize_api_WirelessGetPropertiesReply,
  },
  wirelessConnect: {
    path: '/api.NetctlFront/WirelessConnect',
    requestStream: false,
    responseStream: false,
    requestType: netctl_pb.WirelessConnectRequest,
    responseType: netctl_pb.WirelessConnectReply,
    requestSerialize: serialize_api_WirelessConnectRequest,
    requestDeserialize: deserialize_api_WirelessConnectRequest,
    responseSerialize: serialize_api_WirelessConnectReply,
    responseDeserialize: deserialize_api_WirelessConnectReply,
  },
  wirelessDisconnect: {
    path: '/api.NetctlFront/WirelessDisconnect',
    requestStream: false,
    responseStream: false,
    requestType: netctl_pb.WirelessDisconnectRequest,
    responseType: netctl_pb.WirelessDisconnectReply,
    requestSerialize: serialize_api_WirelessDisconnectRequest,
    requestDeserialize: deserialize_api_WirelessDisconnectRequest,
    responseSerialize: serialize_api_WirelessDisconnectReply,
    responseDeserialize: deserialize_api_WirelessDisconnectReply,
  },
};

exports.NetctlFrontClient = grpc.makeGenericClientConstructor(NetctlFrontService);
