// Copyright 2019 Assured Information Security, Inc. All Rights Reserved.

import React from 'react';
import ReactDOM from 'react-dom';

import App from './components/app';

ReactDOM.render(<App />, document.getElementById('app'));
