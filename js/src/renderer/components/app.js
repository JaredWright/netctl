// Copyright 2019 Assured Information Security, Inc. All Rights Reserved.

import React, {Component} from 'react';

import NetworkList from './network_list';

export default class App extends Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        return (
            <NetworkList/>
        );
    }
}
