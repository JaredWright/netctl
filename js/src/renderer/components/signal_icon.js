import React from 'react';

import SignalExcellent from '../icons/network-wireless-signal-excellent-symbolic.svg';
import SignalGood from '../icons/network-wireless-signal-good-symbolic.svg';
import SignalOK from '../icons/network-wireless-signal-ok-symbolic.svg';
import SignalWeak from '../icons/network-wireless-signal-weak-symbolic.svg';
import SignalNone from '../icons/network-wireless-signal-none-symbolic.svg';

const getIcon = (name) => {
  switch (name.toLowerCase()) {
    case "excellent":
      return (
          <SignalExcellent width='32px' height='32px'/>
      );
    // TODO: use "good" instead
    case "very good":
      return (
          <SignalGood width='32px' height='32px'/>
      );
    // TODO: Use "ok" instead
    case "fair":
      return (
          <SignalOK width='32px' height='32px'/>
      );
    case "weak":
      return (
          <SignalWeak width='32px' height='32px'/>
      );
    case "none":
      return (
          <SignalNone width='32px' height='32px'/>
      );
    default:
      return (
          <SignalNone width='32px' height='32px'/>
      );
  }
};

const SignalIcon = ({
    name = "",
}) => (
    <div className={'network-signal-icon'}>
        {getIcon(name)}
    </div>
);

export default SignalIcon;
