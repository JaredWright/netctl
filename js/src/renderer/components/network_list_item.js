import React, {Component} from 'react';

import SignalIcon from './signal_icon';

// A NetworkListItem is an item in a list of available networks
// displayed by a WifiMenu.
export default class NetworkListItem extends Component {
    constructor(props) {
        super(props);

        this.onItemClicked = this.onItemClicked.bind(this);
    }

    onItemClicked() {
        this.props.itemClicked({...this.props});
    }

    render() {
        let securityLabel;

        switch (this.props.security) {
            case 'wpa-psk':
            case 'wpa-eap':
                securityLabel = 'Secured';
                break;

            case 'unknown':
                securityLabel = 'Open';
                break;
        }

        // Annotate if this network is currently connected.
        if (this.props.isConnected) {
            securityLabel = 'Connected, ' + securityLabel.toLowerCase();
        }

        // If the SSID is empty, display the network as 'Hidden Network'
        const ssidLabel = this.props.ssid ? this.props.ssid : 'Hidden Network';

        let component = (
                <div className={'network-list-item'} onClick={this.onItemClicked}>
                    <SignalIcon name={this.props.signal} />
                    <div className={'network-ssid-label'}>{ssidLabel}
                        <div className={'network-security-label'}>{securityLabel}</div>
                    </div>
                </div>
        );

        return component;
    }
}
