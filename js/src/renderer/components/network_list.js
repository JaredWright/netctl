import React, {Component} from 'react';

import NetworkListItem from './network_list_item';
import NetworkConnectionDialog from './network_connection_dialog';
import ConnectedNetworkDialog from './connected_network_dialog';

import {ipcRenderer} from 'electron';

export default class NetworkList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            // List if NetworkListItem's to render. This is updated
            // on the 'update-available-networks' event.
            networks: [],

            // The network properties and dialog component of the
            // currently (if any) selected network in the list.
            // This is set when a NetworkListItem calls itemClicked,
            // or possibly when handling the 'network-connected' event.
            selectedNetwork: {
                dialog: undefined,
                ssid: undefined,
                signal: undefined,
                security: undefined,
            },

            // The information of the currently (if any) connected
            // network in the list. This is set when a connection
            // dialog calls handleNetworkConnected.
            connectedNetwork: undefined
        };

        this.networkSelected = this.networkSelected.bind(this);
        this.networkUnselected = this.networkUnselected.bind(this);

        // Bind and register event handlers.
        this.handleUpdateAvailableNetworks = this.handleUpdateAvailableNetworks.bind(this);
        this.handleNetworkConnected = this.handleNetworkConnected.bind(this);
        this.handleNetworkDisconnected = this.handleNetworkDisconnected.bind(this);

        ipcRenderer.on('update-available-networks', this.handleUpdateAvailableNetworks);
        ipcRenderer.on('network-connected', this.handleNetworkConnected);
        ipcRenderer.on('network-disconnected', this.handleNetworkDisconnected);
    }

    componentDidMount() {
        // Notify the main process that this component is ready.
        ipcRenderer.send('network-list-ready', null);
    }

    handleUpdateAvailableNetworks(event, arg) {
        this.setState((state) => ({ ...state, networks: arg}));
    }

    handleNetworkConnected(event, arg) {
        // If the network that just became connected is also currently
        // selected in the list, change the dialog to a ConnectedNetworkDialog.
        if (this.state.selectedNetwork && arg === this.state.selectedNetwork.ssid) {
            let dialog = (
                <ConnectedNetworkDialog
                    key={this.state.selectedNetwork.ssid}
                    ssid={this.state.selectedNetwork.ssid}
                    signal={this.state.selectedNetwork.signal}
                    security={this.state.selectedNetwork.security}
                    itemClicked={this.networkUnselected}
                />
            );

            this.setState((state) => ({
                ...state,
                connectedNetwork: arg,
                selectedNetwork: {
                    ...state.selectedNetwork,
                    dialog: dialog,
                }
            }));

            return
        }

        this.setState((state) => ({...state, connectedNetwork: arg}));
    }

    handleNetworkDisconnected(event, arg) {
        this.setState((state) => ({...state, connectedNetwork: undefined }));
    }

    networkSelected(net) {
        let dialog = (
            <NetworkConnectionDialog
                key={net.ssid}
                ssid={net.ssid}
                signal={net.signal}
                security={net.security}
                itemClicked={this.networkUnselected}
            />
        );

        // If this selected network is also the connected
        // network, render a ConnectedNetworkDialog instead.
        if (net.ssid === this.state.connectedNetwork) {
            dialog = (
                <ConnectedNetworkDialog
                    key={net.ssid}
                    ssid={net.ssid}
                    signal={net.signal}
                    security={net.security}
                    itemClicked={this.networkUnselected}
                />
            );
        }

        this.setState((state) => ({
            ...state,
            selectedNetwork: {
                dialog: dialog,
                ssid: net.ssid,
                signal: net.signal,
                security: net.security,
            },
        }));
    }

    networkUnselected() {
        this.setState((state) => ({ ...state, selectedNetwork: undefined }));
    }

    render() {
        // If the network list is empty, display a message.
        if (this.state.networks.length === 0) {
            return (
                <div className='info-display'>
                    Searching for WiFi networks...
                </div>
            );
        }

        let networkListItems = [];

        this.state.networks.forEach((net) => {
            let item = (
                <NetworkListItem
                    key={net.ssid}
                    ssid={net.ssid}
                    signal={net.signal}
                    security={net.security}
                    isConnected={net.ssid === this.state.connectedNetwork}
                    itemClicked={this.networkSelected}
                />
            );

            // If this network is the selected network item, render
            // the saved component instead.
            if (this.state.selectedNetwork && net.ssid === this.state.selectedNetwork.ssid) {
                item = this.state.selectedNetwork.dialog;
            }

            // If this network item corresponds to the connected
            // network, render it at the top of the list.
            if (net.ssid === this.state.connectedNetwork) {
                networkListItems.unshift(item);
            } else {
                networkListItems.push(item);
            }
        });

        let component = (
            <div className={'network-list'}>
                {networkListItems}
            </div>
        );

        return component;
    }
}
