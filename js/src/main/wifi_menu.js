const grpc = require('grpc');
const path = require('path');

const {BrowserWindow,Tray,nativeImage,ipcMain,screen} = require('electron');

const netctl = require('../api/netctl');

// The BrowserWindow options used to create a WifiWindow.
const wifiWindowOptions = {
    width: 320,
    height: 450,
    show: false,
    frame: false,
    fullscreenable: false,
    resizable: false,
    transparent: false,
    skipTaskbar: true,
    webPreferences: {
      backgroundThrottling: false,
      nodeIntegration: true,
    }
};

class WifiMenu {
    constructor(address) {
        this.address = address;

        // Create a BrowserWindow with the wifiWindowOptions.
        this.win = new BrowserWindow(wifiWindowOptions);

        // Bind window controlling functions.
        this.show = this.show.bind(this);
        this.getPosition = this.getPosition.bind(this);

        const icon = path.join(__dirname, '../assets/icons/network-wireless-signal-none-symbolic.ico');

        this.tray = new Tray(nativeImage.createFromPath(icon));

        // These event handlers create the 'toggle' behavior for the
        // tray icon.
        this.tray.on('click', (event) => {
            this.win.isVisible() ? this.win.hide() : this.show();
        });

        this.win.on('blur', () => {
            // If the 'blur' event occurs when the mouse is in the bounds
            // of the tray icon, ignore the event and let the 'click' event
            // handle it.
            const cursorPoint = screen.getCursorScreenPoint();

            if (this.isPointInTray(cursorPoint)) {
                return;
            }

            this.win.hide();
        });

        this.handleConnectEvent = this.handleConnectEvent.bind(this);
        this.handleDisconnectEvent = this.handleDisconnectEvent.bind(this);

        // When the network list becomes ready, initialize the client.
        // This event will always be triggered within some reasonable
        // time constraint since it only depends on the render process
        // mounting a NetworkList component. This helps simplify the
        // logic of initializing the client, and subesequently giving the
        // NetworkList its initial state.
        this.initializeClient = this.initializeClient.bind(this);
        ipcMain.on('network-list-ready', this.initializeClient);

        // Finally, load the render process.
        this.win.loadURL(`file://${path.join(__dirname,'index.html')}`)
    }

    initializeClient() {
        // Create the netctl frontend client.
        this.client = new netctl.clients.NetctlFrontClient(
            this.address,
            grpc.credentials.createInsecure()
        );

        grpc.waitForClientReady(this.client, Date.now() + (3 * 1000), (err) => {
            if (err) {
                console.log('Failed to initialize netctl client, retrying... (err=' + err + ')');

                setTimeout(this.initializeClient, 2000);
            } else {
                // Bind IPC event handlers...
                ipcMain.on('connect', this.handleConnectEvent);
                ipcMain.on('disconnect', this.handleDisconnectEvent);

                // ... send network list initial properties...
                this.initializeNetworkListProperties();

                // ... and start properties monitor.
                this.makePropertiesMonitor();

                console.log('Initialized netctl client');
            }
        });
    }

    show() {
        const pos = this.getPosition();

        this.win.setPosition(pos.x, pos.y, false);
        this.win.show();
    }

    isPointInTray(point) {
        const trayBounds = this.tray.getBounds();

        const xInRange = (point.x >= trayBounds.x && point.x <= (trayBounds.x + trayBounds.width));
        const yInRange = (point.y >= trayBounds.y && point.y <= (trayBounds.y + trayBounds.height));

        return (xInRange && yInRange);
    }

    getPosition() {
        const tb = this.tray.getBounds();
        const wb = this.win.getBounds();

        // Center the window around the tray entry.
        const x = Math.round(tb.x + (tb.width / 2) - (wb.width / 2));

        // Add a small pixel buffer between the bottom of the window
        // and the top of the tray.
        const y = Math.round(tb.y - wb.height + 4);

        return {x: x, y: y};
    }

    initializeNetworkListProperties() {
        let req = new netctl.types.WirelessGetPropertiesRequest();

        // When the NetworkList component is ready, get initial wireless
        // properties from the netctl frontend and set initial state.
        this.client.wirelessGetProperties(req, (err, resp) => {
            if (err) {
                console.log(err);
                return;
            }

            const props = resp.getProps();

            // Set the signal icon...
            this.setSignalIcon(props.getSignalStrength());

            // Set initial available networks list...
            this.updateAvailableNetworksNotify(props.getAccessPointsList());

            // If there is already an associated network, set the SSID.
            const ssid = props.getSsid();
            if (ssid) {
                this.win.webContents.send('network-connected', ssid);
            }
        });
    }

    makePropertiesMonitor() {
        let stream = this.client.wirelessMonitorProperties(
            new netctl.types.WirelessMonitorPropertiesRequest()
        );

        // Create a shorthand for wireless properties update
        // types enum.
        const type = netctl.types.WirelessPropertiesUpdate.Type;

        stream.on('data', (data) => {
            const update = data.getUpdate();

            switch(update.getType()) {
            case type.ACCESSPOINTS:
                // The list of available networks has been updated, notify
                // the render process.
                const networks = update.getProps().getAccessPointsList();

                this.updateAvailableNetworksNotify(networks);

                break;

            case type.IP:
                // When the IP property changes, the SSID is set along with it.
                // Use this to tell the render process which network is connected,
                // since it only needs the SSID of the connected network to know
                // how to render the network list.
                const ssid = update.getProps().getSsid();

                if (ssid) {
                    this.win.webContents.send('network-connected', ssid);
                }

                break;

            case type.STATE:
                const state = update.getProps().getState();

                this.networkStateChangeNotify(state);

                break;

            case type.SIGNAL:
                const signal = update.getProps().getSignalStrength();

                this.setSignalIcon(signal);

                break;
            }
        });
    }

    updateAvailableNetworksNotify(networks) {
        let available = [];

        networks.forEach((n) => {
            let network = {
                ssid: n.getSsid(),
                signal: n.getSignalStrength(),
                security: n.getKeyMgmt()
            };

            available.push(network);
        });

        this.win.webContents.send('update-available-networks', available);
    }

    networkStateChangeNotify(state) {
        switch (state){
            // The following states only impact how the tray icon is
            // set. The render process does not need to be informed
            // about these.
            case netctl.types.WirelessState.CONNECTED:
            case netctl.types.WirelessState.CONNECTING:
            case netctl.types.WirelessState.UNKNOWN:
                // TODO
                break;

            case netctl.types.WirelessState.FAILED:
                // This case will be directly handled by the
                // wirelessConnect caller.
                break;

            case netctl.types.WirelessState.DISCONNECTED:
                // The render process needs to know when a network is
                // disconnected so that it can update the network list
                // state.
                this.win.webContents.send('network-disconnected', null);

                break;
        }
    }

    setSignalIcon(signal) {
        const dir = path.join(__dirname, '../assets/icons');

        let icon;

        switch (signal) {
            case 'Excellent':
                icon = path.join(dir, 'network-wireless-signal-excellent-symbolic.ico');
                break;

            case 'Very Good':
                icon = path.join(dir, 'network-wireless-signal-good-symbolic.ico');
                break;

            case 'Fair':
                icon = path.join(dir, 'network-wireless-signal-ok-symbolic.ico');
                break;

            case 'Weak':
            case 'Very Weak':
                icon = path.join(dir, 'network-wireless-signal-weak-symbolic.ico');
                break;

            default:
                icon = path.join(dir, 'network-wireless-signal-none-symbolic.ico');
        }

        this.tray.setImage(icon);
    }

    handleConnectEvent(event, arg) {
        // Build the network configuration from arg.
        let cfg = new netctl.types.WirelessNetworkConfiguration();

        // Set the SSID name.
        cfg.setSsid(arg.ssid);

        // Set scanSsid if necessary
        if (arg.scanSSID) {
            cfg.setScanSsid(1);
        }

        // Set the auth data according to the 'security' of
        // the network.
        switch (arg.security) {
            case 'wpa-psk':
                cfg.setKeyMgmt(arg.security.toUpperCase());
                cfg.setPsk(arg.password);
                break;

            case 'wpa-eap':
                // TODO
                break;

            case 'unknown':
                cfg.setKeyMgmt('NONE')
                break;
        }

        // Make the request to netctl.
        let req = new netctl.types.WirelessConnectRequest();
        req.setConfig(cfg);

        this.client.wirelessConnect(req, (err, resp) => {
            if (err) {
                this.win.webContents.send('connection-failed');

                console.log(err);
                return;
            }

            if (resp.getState() !== netctl.types.WirelessState.CONNECTED) {
                this.win.webContents.send('connection-failed');
            }
        });
    }

    handleDisconnectEvent(event, arg) {
        let req = new netctl.types.WirelessDisconnectRequest();

        this.client.wirelessDisconnect(req, (err, resp) => {
            if (err) {
                console.log(err);
            }
        });
    }
}

module.exports = WifiMenu;
