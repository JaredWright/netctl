const {app} = require('electron');
const path = require('path');
const process = require('process');

const WifiMenu = require('./wifi_menu');

let wifiMenu

app.on('ready', () => {
    wifiMenu = new WifiMenu(process.env.NETCTL_FRONTEND_ADDRESS);
})
