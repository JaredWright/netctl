const webpack = require('webpack');

module.exports = {
    target: 'electron-renderer',
    entry: './src/renderer/index.js',

    output: {
        path: __dirname + '/src/main',
        filename: 'render.js'
    },

    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                loader: 'babel-loader',
                options: {
                    presets: ['@babel/react']
                }
            },
            {
                test: /\.svg$/,
                use: ['@svgr/webpack']
            }
        ]
    },

    resolve: {
      extensions: ['.js', '.json', '.jsx']
    }
}
